# Blockchains and Cryptocurrencies in the Real world

Produced for educational purposes from permissively-licensed materials

## Table of Contents

1. Blockchain Basics
   - References and further Reading for Chapter 1
2. Blockchain Uses in the Real World
   - References and further Reading for Chapter 2
3. Cryptocurrency Basics
   - References and further Reading for Chapter 3
4. Regulation of Cryptocurrencies
   - References and further Reading for Chapter 4
5. Deep Look at Bitcoin
   - References and further Reading for Chapter 5
6. Deep Look at Ethereum
   - References and further Reading for Chapter 6
7. Deep Look at NEM
   - References and further Reading for Chapter 7
8. Smart Contracts
   - References and further Reading for Chapter 8
9. A Look at Cryptokitties
   - References and further Reading for Chapter 9
10. Appendix 1 List of Cryptocurrencies
11. Appendix 2 Legality of Cryptocurrencies by Country
12. Glossary

# 1. Blockchain Basics

![Block Formation Diagram](./images/block-formation.png "Block Formation Diagram")

A blockchain is a decentralized, distributed, and oftentimes public, digital ledger that is used to record transactions across many computers so that any involved record cannot be altered retroactively, without the alteration of all subsequent blocks. This allows the participants to verify and audit transactions independently and relatively inexpensively. A blockchain database is managed autonomously using a peer-to-peer network and a distributed timestamping server. They are authenticated by mass collaboration powered by collective self-interests. Such a design facilitates robust workflow where participants' uncertainty regarding data security is marginal.

By design, a blockchain is resistant to modification of the data. It is "an open, distributed ledger that can record transactions between two parties efficiently and in a verifiable and permanent way". For use as a distributed ledger, a blockchain is typically managed by a peer-to-peer network collectively adhering to a protocol for inter-node communication and validating new blocks. Once recorded, the data in any given block cannot be altered retroactively without alteration of all subsequent blocks, which requires consensus of the network majority. Although blockchain records are not unalterable, blockchains may be considered secure by design and exemplify a distributed computing system with high Byzantine fault tolerance. Decentralized consensus has therefore been claimed with a blockchain.

The first work on a cryptographically secured chain of blocks was described in 1991 by Stuart Haber and W. Scott Stornetta. They wanted to implement a system where document timestamps could not be tampered with. In 1992, Bayer, Haber and Stornetta incorporated Merkle trees to the design, which improved its efficiency by allowing several document certificates to be collected into one block.

![Bitcoin a peer to peer](./images/bitcoin-a-peer-to-peer.png "Bitcoin a peer to peer")

The first blockchain was conceptualized by a person (or group of people) known as Satoshi Nakamoto in 2008. Nakamoto improved the design in an important way using a Hashcash-like method to timestamp blocks without requiring them to be signed by a trusted party and to reduce speed with which blocks are added to the chain. The design was implemented the following year by Nakamoto as a core component of the cryptocurrency bitcoin, where it serves as the public ledger for all transactions on the network.

The words block and chain were used separately in Satoshi Nakamoto's original paper, but were eventually popularized as a single word, blockchain, by 2016.

The use of a blockchain removes the characteristic of infinite reproducibility from a digital asset. It confirms that each unit of value was transferred only once, solving the long-standing problem of double spending. A blockchain has been described as a value-exchange protocol. A blockchain can maintain title rights because, when properly set up to detail the exchange agreement, it provides a record that compels offer and acceptance.

![Transaction Block](./images/block.png "Transaction Block")

Blocks hold batches of valid transactions that are hashed and encoded into a Merkle tree. Each block includes the cryptographic hash of the prior block in the blockchain, linking the two. The linked blocks form a chain. This iterative process confirms the integrity of the previous block, all the way back to the original genesis block.

![Transaction Blocks](./images/blocks.png "Transaction Blocks")

Sometimes separate blocks can be produced concurrently, creating a temporary fork. In addition to a secure hash-based history, any blockchain has a specified algorithm for scoring different versions of the history so that one with a higher score can be selected over others. Blocks not selected for inclusion in the chain are called orphan blocks. Peers supporting the database have different versions of the history from time to time. They keep only the highest-scoring version of the database known to them.

Whenever a peer receives a higher-scoring version (usually the old version with a single new block added) they extend or overwrite their own database and retransmit the improvement to their peers. There is never an absolute guarantee that any particular entry will remain in the best version of the history forever. Blockchains are typically built to add the score of new blocks onto old blocks and are given incentives to extend with new blocks rather than overwrite old blocks.

Therefore, the probability of an entry becoming superseded decreases exponentially as more blocks are built on top of it, eventually becoming very low. For example, bitcoin uses a proof-of-work system, where the chain with the most cumulative proof-of-work is considered the valid one by the network. There are a number of methods that can be used to demonstrate a sufficient level of computation. Within a blockchain the computation is carried out redundantly rather than in the traditional segregated and parallel manner.

The block time is the average time it takes for the network to generate one extra block in the blockchain. Some blockchains create a new block as frequently as every five seconds. By the time of block completion, the included data becomes verifiable. In cryptocurrency, this is practically when the transaction takes place, so a shorter block time means faster transactions. The block time for Ethereum is set to between 14 and 15 seconds, while for bitcoin it is on average 10 minutes.

A hard fork is a rule change such that the software validating according to the old rules will see the blocks produced according to the new rules as invalid. In case of a hard fork, all nodes meant to work in accordance with the new rules need to upgrade their software.

![Transaction Blocks-2](./images/blocks-2.png "Transaction Blocks-2")

If one group of nodes continues to use the old software while the other nodes use the new software, a permanent split can occur. For example, Ethereum has hard-forked to "make whole" the investors in The DAO, which had been hacked by exploiting a vulnerability in its code. In this case, the fork resulted in a split creating Ethereum and Ethereum Classic chains.

By storing data across its peer-to-peer network, the blockchain eliminates a number of risks that come with data being held centrally. The decentralized blockchain may use ad hoc message passing and distributed networking.

![Decentralization](./images/decentralization.png "Decentralization")

Peer-to-peer blockchain networks lack centralized points of vulnerability that computer crackers can exploit; likewise, it has no central point of failure. Blockchain security methods include the use of public-key cryptography. A public key (a long, random-looking string of numbers) is an address on the blockchain. Value tokens sent across the network are recorded as belonging to that address. A private key is like a password that gives its owner access to their digital assets or the means to otherwise interact with the various capabilities that blockchains now support. Data stored on the blockchain is generally considered incorruptible.

Every node in a decentralized system has a copy of the blockchain. Data quality is maintained by massive database replication and computational trust. No centralized "official" copy exists and no user is "trusted" more than any other. Transactions are broadcast to the network using software. Messages are delivered on a best-effort basis.

Mining nodes validate transactions, add them to the block they are building, and then broadcast the completed block to other nodes. Blockchains use various time-stamping schemes, such as proof-of-work, to serialize changes. Alternative consensus methods include proof-of-stake. Growth of a decentralized blockchain is accompanied by the risk of centralization because the computer resources required to process larger amounts of data become more expensive.

Open blockchains are more user-friendly than some traditional ownership records, which, while open to the public, still require physical access to view. Because all early blockchains were permissionless, controversy has arisen over the blockchain definition. An issue in this ongoing debate is whether a private system with verifiers tasked and authorized (permissioned) by a central authority should be considered a blockchain. Proponents of permissioned or private chains argue that the term "blockchain" may be applied to any data structure that batches data into time-stamped blocks.

Just as multiversion concurrency control prevents two transactions from concurrently modifying a single object in a database, blockchains prevent two transactions from spending the same single output in a blockchain.

The great advantage to an open, permissionless, or public, blockchain network is that guarding against bad actors is not required and no access control is needed. This means that applications can be added to the network without the approval or trust of others, using the blockchain as a transport layer.

Bitcoin and other cryptocurrencies currently secure their blockchain by requiring new entries to include a proof of work. To prolong the blockchain, bitcoin uses Hashcash puzzles. While Hashcash was designed in 1997 by Adam Back, the original idea was first proposed by Cynthia Dwork and Moni Naor and Eli Ponyatovski in their 1992 paper "Pricing via Processing or Combatting Junk Mail".

Permissioned blockchains use an access control layer to govern who has access to the network. In contrast to public blockchain networks, validators on private blockchain networks are vetted by the network owner. They do not rely on anonymous nodes to validate transactions nor do they benefit from the network effect.[citation needed] Permissioned blockchains can also go by the name of 'consortium' blockchains.

The analysis of public blockchains has become increasingly important with the popularity of bitcoin, Ethereum, litecoin and other cryptocurrencies. A blockchain, if it is public, provides anyone who wants access to observe and analyse the chain data, given one has the know-how. The process of understanding and accessing the flow of crypto has been an issue for many cryptocurrencies, crypto-exchanges and banks.

The reason for this is accusations of blockchain enabled cryptocurrencies enabling illicit dark market trade of drugs, weapons, money laundering etc. A common belief has been that cryptocurrency is private and untraceable, thus leading many actors to use it for illegal purposes. This is changing and now specialised tech-companies provide blockchain tracking services, making crypto exchanges, law-enforcement and banks more aware of what is happening with crypto funds and fiat crypto exchanges.

The development, some argue, has led criminals to prioritise use of new cryptocurrencies such as Monero. The question is about public accessibility of blockchain data and the personal privacy of the very same data. It is a key debate in cryptocurrency and ultimately in blockchain.

Blockchain technology can be integrated into multiple areas. The primary use of blockchains today is as a distributed ledger for cryptocurrencies, most notably bitcoin. There are a few operational products maturing from proof of concept by late 2016. Businesses have been thus far reluctant to place blockchain at the core of the business structure.

Most cryptocurrencies use blockchain technology to record transactions. For example, the bitcoin network and Ethereum network are both based on blockchain. On 8 May 2018 Facebook confirmed that it would open a new blockchain group which would be headed by David Marcus, who previously was in charge of Messenger. Facebook's planned cryptocurrency platform, Libra, was formally announced on June 18, 2019.

Blockchain-based smart contracts are proposed contracts that can be partially or fully executed or enforced without human interaction. One of the main objectives of a smart contract is automated escrow. An IMF staff discussion reported that smart contracts based on blockchain technology might reduce moral hazards and optimize the use of contracts in general. But "no viable smart contract systems have yet emerged." Due to the lack of widespread use their legal status is unclear.

Major portions of the financial industry are implementing distributed ledgers for use in banking, and according to a September 2016 IBM study, this is occurring faster than expected. Banks are interested in this technology because it has potential to speed up back office settlement systems. Banks such as UBS are opening new research labs dedicated to blockchain technology in order to explore how blockchain can be used in financial services to increase efficiency and reduce costs.

## References and further Reading for Chapter 1

[https://web.archive.org/web/20160703000844/](https://web.archive.org/web/20160703000844/)

[https://www.economist.com/news/briefing/21677228-technology-behind-bitcoin-lets-people-who-do-not-know-or-trust-each-other-build-dependable](https://www.economist.com/news/briefing/21677228-technology-behind-bitcoin-lets-people-who-do-not-know-or-trust-each-other-build-dependable)

[https://web.archive.org/web/20160521015602/](https://web.archive.org/web/20160521015602/)

[http://fortune.com/2016/05/15/leaderless-blockchain-vc-fund/](http://fortune.com/2016/05/15/leaderless-blockchain-vc-fund/)

[https://web.archive.org/web/20160522034932/](https://web.archive.org/web/20160522034932/)

[https://www.nytimes.com/2016/05/22/business/dealbook/crypto-ether-bitcoin-currency.html](https://www.nytimes.com/2016/05/22/business/dealbook/crypto-ether-bitcoin-currency.html)

[https://web.archive.org/web/20130921060724/](https://web.archive.org/web/20130921060724/)

[http://mercatus.org/sites/default/files/Brito_BitcoinPrimer.pdf](http://mercatus.org/sites/default/files/Brito_BitcoinPrimer.pdf)

[https://web.archive.org/web/20160417111508/](https://web.archive.org/web/20160417111508/)

[https://github.com/trottier/original-bitcoin/blob/master/src/main.h#L795-L803](https://github.com/trottier/original-bitcoin/blob/master/src/main.h#L795-L803)

**Narayanan, Arvind; Bonneau, Joseph; Felten, Edward; Miller, Andrew; Goldfeder, Steven (2016). Bitcoin and cryptocurrency technologies: a comprehensive introduction. Princeton: Princeton University Press. ISBN 978-0-691-17169-2.**

[https://web.archive.org/web/20170118052537/](https://web.archive.org/web/20170118052537/)

[https://hbr.org/2017/01/the-truth-about-blockchain](https://hbr.org/2017/01/the-truth-about-blockchain)

[https://books.google.com/books?id=fvywDAAAQBAJ](https://books.google.com/books?id=fvywDAAAQBAJ)

[https://dailyfintech.com/2018/02/10/bitcoin-will-finally-disrupt-the-credit-card-rails/](https://dailyfintech.com/2018/02/10/bitcoin-will-finally-disrupt-the-credit-card-rails/)

_Haber, Stuart; Stornetta, W. Scott (January 1991). "How to time-stamp a digital document". Journal of Cryptology. 3 (2): 99–111. CiteSeerX 10.1.1.46.8740. doi:10.1007/bf00196791._

_Bayer, Dave; Haber, Stuart; Stornetta, W. Scott (March 1992). Improving the Efficiency and Reliability of Digital Time-Stamping. Sequences. 2. pp. 329–334. CiteSeerX 10.1.1.71.4891. doi:10.1007/978-1-4613-9323-8_24. ISBN 978-1-4613-9325-2._

[https://web.archive.org/web/20161108130946/](https://web.archive.org/web/20161108130946/)

[http://www.wired.co.uk/article/unlock-the-blockchain](http://www.wired.co.uk/article/unlock-the-blockchain)

[http://www.nber.org/papers/w22952.pdf](http://www.nber.org/papers/w22952.pdf)

[https://web.archive.org/web/20161113134748/](https://web.archive.org/web/20161113134748/)

[http://fortune.com/2016/05/08/why-blockchains-will-change-the-world/](http://fortune.com/2016/05/08/why-blockchains-will-change-the-world/)

[https://web.archive.org/web/20161113134748/](https://web.archive.org/web/20161113134748/)

[https://web.archive.org/web/20161114001509/](https://web.archive.org/web/20161114001509/)

[https://www.wired.com/insights/2015/01/block-chain-2-0/](https://www.wired.com/insights/2015/01/block-chain-2-0/)

_Bhaskar, Nirupama Devi; Chuen, David LEE Kuo (2015). "Bitcoin Mining Technology". Handbook of Digital Currency. pp. 45–65. doi:10.1016/B978-0-12-802117-0.00003-5. ISBN 978-0-12-802117-0._

[https://web.archive.org/web/20161031132328/](https://web.archive.org/web/20161031132328/)

[http://radar.oreilly.com/2014/02/bitcoin-security-model-trust-by-computation.html](http://radar.oreilly.com/2014/02/bitcoin-security-model-trust-by-computation.html)

[https://web.archive.org/web/20161201131627/](https://web.archive.org/web/20161201131627/)

[http://chimera.labs.oreilly.com/books/1234000001802/ch01.html](http://chimera.labs.oreilly.com/books/1234000001802/ch01.html)

[https://bitcoin.org/bitcoin.pdf](https://bitcoin.org/bitcoin.pdf)

[https://web.archive.org/web/20161120154948/](https://web.archive.org/web/20161120154948/)

[https://monax.io/explainers/permissioned_blockchains/](https://monax.io/explainers/permissioned_blockchains/)

[https://arstechnica.com/business/2013/03/major-glitch-in-bitcoin-network-sparks-sell-off-price-temporarily-falls-23/](https://arstechnica.com/business/2013/03/major-glitch-in-bitcoin-network-sparks-sell-off-price-temporarily-falls-23/)

[https://www.webcitation.org/6G4e02Xd1?url=http://arstechnica.com/business/2013/03/major-glitch-in-bitcoin-network-sparks-sell-off-price-temporarily-falls-23/](https://www.webcitation.org/6G4e02Xd1?url=http://arstechnica.com/business/2013/03/major-glitch-in-bitcoin-network-sparks-sell-off-price-temporarily-falls-23/)

[https://web.archive.org/web/20141231031044/http://www.newyorker.com/tech/elements/the-mission-to-decentralize-the-internet](https://web.archive.org/web/20141231031044/http://www.newyorker.com/tech/elements/the-mission-to-decentralize-the-internet)

[https://web.archive.org/web/20161010042659/https://www.infoq.com/articles/is-bitcoin-a-decentralized-currency/](https://web.archive.org/web/20161010042659/https://www.infoq.com/articles/is-bitcoin-a-decentralized-currency/)

[https://web.archive.org/web/20151101235750/http://moneyandstate.com/its-all-about-the-blockchain/](https://web.archive.org/web/20151101235750/http://moneyandstate.com/its-all-about-the-blockchain/)

[https://web.archive.org/web/20160421112045/http://www.paymentssource.com/news/technology/a-very-public-confluct-over-private-blockchains-3021831-1.html](https://web.archive.org/web/20160421112045/http://www.paymentssource.com/news/technology/a-very-public-confluct-over-private-blockchains-3021831-1.html)

[https://web.archive.org/web/20160629234654/http://news.dinbits.com/2015/11/the-blockchain-technology-bandwagon-has.html](https://web.archive.org/web/20160629234654/http://news.dinbits.com/2015/11/the-blockchain-technology-bandwagon-has.html)

[https://web.archive.org/web/20160330060709/http://www.americanbanker.com/bankthink/why-the-bitcoin-blockchain-beats-out-competitors-1075100-1.html](https://web.archive.org/web/20160330060709/http://www.americanbanker.com/bankthink/why-the-bitcoin-blockchain-beats-out-competitors-1075100-1.html)

[https://web.archive.org/web/20160608070620/http://www.multichain.com/blog/2015/07/bitcoin-vs-blockchain-debate/](https://web.archive.org/web/20160608070620/http://www.multichain.com/blog/2015/07/bitcoin-vs-blockchain-debate/)

_Tapscott, Don; Tapscott, Alex (May 2016). The Blockchain Revolution: How the Technology Behind Bitcoin is Changing Money, Business, and the World. ISBN 978-0-670-06997-2._

[https://web.archive.org/web/20161130143727/https://channels.theinnovationenterprise.com/articles/blockchain-top-trends-in-2017](https://web.archive.org/web/20161130143727/https://channels.theinnovationenterprise.com/articles/blockchain-top-trends-in-2017)

[https://web.archive.org/web/20170925180800/http://au.pcmag.com/amazon-web-services/46389/feature/blockchain-the-invisible-technology-thats-changing-the-world
https://web.archive.org/web/20180119120102/https://blockchainhub.net/blockchains-and-distributed-ledger-technologies-in-general/](https://web.archive.org/web/20170925180800/http://au.pcmag.com/amazon-web-services/46389/feature/blockchain-the-invisible-technology-thats-changing-the-world
https://web.archive.org/web/20180119120102/https://blockchainhub.net/blockchains-and-distributed-ledger-technologies-in-general/)

[https://cdn.crowdfundinsider.com/wp-content/uploads/2017/04/Global-Cryptocurrency-Benchmarking-Study.pdf](https://cdn.crowdfundinsider.com/wp-content/uploads/2017/04/Global-Cryptocurrency-Benchmarking-Study.pdf)

[https://www.ingentaconnect.com/content/hsp/jpss/2015/00000009/00000001/art00005](https://www.ingentaconnect.com/content/hsp/jpss/2015/00000009/00000001/art00005)

[https://www.bloomberg.com/news/articles/2019-03-03/why-crypto-companies-still-can-t-open-checking-accounts](https://www.bloomberg.com/news/articles/2019-03-03/why-crypto-companies-still-can-t-open-checking-accounts)

[https://aisel.aisnet.org/cgi/viewcontent.cgi?article=1019&context=ecis2015_cr](https://aisel.aisnet.org/cgi/viewcontent.cgi?article=1019&context=ecis2015_cr)

[https://www.wired.com/2017/01/monero-drug-dealers-cryptocurrency-choice-fire/](https://www.wired.com/2017/01/monero-drug-dealers-cryptocurrency-choice-fire/)

[https://www.technologyreview.com/s/608763/criminals-thought-bitcoin-was-the-perfect-hiding-place-they-thought-wrong/](https://www.technologyreview.com/s/608763/criminals-thought-bitcoin-was-the-perfect-hiding-place-they-thought-wrong/)

[https://uk.reuters.com/article/us-crypto-currencies-altcoins-explainer-idUKKCN1SL0F0](https://uk.reuters.com/article/us-crypto-currencies-altcoins-explainer-idUKKCN1SL0F0)

[https://magazine.fintechweekly.com/articles/an-untraceable-currency-bitcoin-privacy-concerns](https://magazine.fintechweekly.com/articles/an-untraceable-currency-bitcoin-privacy-concerns)

[https://web.archive.org/web/20161109152317/https://www.ft.com/content/c905b6fc-4dd2-3170-9d2a-c79cdbb24f16](https://web.archive.org/web/20161109152317/https://www.ft.com/content/c905b6fc-4dd2-3170-9d2a-c79cdbb24f16)

[https://www.recode.net/2018/5/8/17330226/facebook-reorg-mark-zuckerberg-whatsapp-messenger-ceo-blockchain](https://www.recode.net/2018/5/8/17330226/facebook-reorg-mark-zuckerberg-whatsapp-messenger-ceo-blockchain)

[https://www.nytimes.com/2019/06/18/technology/facebook-cryptocurrency-libra.html?action=click&module=Top%20Stories&pgtype=Homepage](https://www.nytimes.com/2019/06/18/technology/facebook-cryptocurrency-libra.html?action=click&module=Top%20Stories&pgtype=Homepage)

[https://techcrunch.com/2019/06/18/facebook-libra/](https://techcrunch.com/2019/06/18/facebook-libra/)

[https://web.archive.org/web/20170214204859/https://books.google.com/books?id=YHfCBwAAQBAJ](https://web.archive.org/web/20170214204859/https://books.google.com/books?id=YHfCBwAAQBAJ)

_Governatori, Guido; Idelberger, Florian; Milosevic, Zoran; Riveret, Regis; Sartor, Giovanni; Xu, Xiwei (2018). "On legal contracts, imperative and declarative smart contracts, and blockchain systems". Artificial Intelligence and Law. 26 (4): 33. doi:10.1007/s10506-018-9223-3._

[https://web.archive.org/web/20180414210721/https://www.imf.org/external/pubs/ft/sdn/2016/sdn1603.pdf](https://web.archive.org/web/20180414210721/https://www.imf.org/external/pubs/ft/sdn/2016/sdn1603.pdf)

[https://web.archive.org/web/20160708170429/http://reason.com/reasontv/2016/05/06/bitcoin-consensus-blockchain-wall-street](https://web.archive.org/web/20160708170429/http://reason.com/reasontv/2016/05/06/bitcoin-consensus-blockchain-wall-street)

[https://web.archive.org/web/20160703103852/http://www.afr.com/technology/anz-backs-private-blockchain-but-wont-go-public-20160629-gpuf9z](https://web.archive.org/web/20160703103852/http://www.afr.com/technology/anz-backs-private-blockchain-but-wont-go-public-20160629-gpuf9z)

[https://web.archive.org/web/20180317232141/http://www.postboxcommunications.com/blog/can-banking-sector-leverage-blockchain-technology/](https://web.archive.org/web/20180317232141/http://www.postboxcommunications.com/blog/can-banking-sector-leverage-blockchain-technology/)

[https://web.archive.org/web/20160928163048/http://www.reuters.com/article/us-tech-blockchain-ibm-idUSKCN11Y28D](https://web.archive.org/web/20160928163048/http://www.reuters.com/article/us-tech-blockchain-ibm-idUSKCN11Y28D)

[https://web.archive.org/web/20161109152822/https://www.ft.com/content/719f4e7e-80e1-11e6-bc52-0c7211ef3198](https://web.archive.org/web/20161109152822/https://www.ft.com/content/719f4e7e-80e1-11e6-bc52-0c7211ef3198)

[https://web.archive.org/web/20170519073429/http://www.reuters.com/article/us-banks-blockchain-ubs-idUSKCN10Z147](https://web.archive.org/web/20170519073429/http://www.reuters.com/article/us-banks-blockchain-ubs-idUSKCN10Z147)

[https://web.archive.org/web/20161205202317/https://www.capgemini.com/beyond-the-buzz/cryptocurrency-blockchain](https://web.archive.org/web/20161205202317/https://www.capgemini.com/beyond-the-buzz/cryptocurrency-blockchain)

[https://www.reuters.com/article/us-banks-blockchain-r3/top-banks-and-r3-build-blockchain-based-payments-system-idUSKBN1D00ZB](https://www.reuters.com/article/us-banks-blockchain-r3/top-banks-and-r3-build-blockchain-based-payments-system-idUSKBN1D00ZB)

[https://www2.deloitte.com/content/dam/Deloitte/lu/Documents/technology/lu-token-assets-securities-tomorrow.pdf](https://www2.deloitte.com/content/dam/Deloitte/lu/Documents/technology/lu-token-assets-securities-tomorrow.pdf)

# 2. Blockchain Uses in the Real World

Despite still being considered in the “cutting edge” or “early adopter” phase, there are already a number of real-world uses of blockchains besides cryptocurrencies. These include:

**Cryptokitties**, A blockchain game launched in November 2017. The game illustrated scalability problems for games on Ethereum when it created significant congestion on the Ethereum network with about 30% of all Ethereum transactions being for the game. CryptoKitties also demonstrated how blockchains can be used to catalog game assets (digital assets).

![Crypto Kitties](./images/crypto-kitties.png "Crypto Kitties")

**Everledger**, a collaboration between Walmart and IBM to use a blockchain-backed system for supply chain monitoring, currently in testing

**Mycelia**, a service which has also been proposed as blockchain-based alternative to traditional music distribution "that gives artists more control over how their songs and associated data circulate among fans and other musicians."

**Hyperledger**, a cross-industry collaborative effort from the Linux Foundation to support blockchain-based distributed ledgers.
Quorum, a permissionable private blockchain by JPMorgan Chase.
**Tezos**, used for decentralized voting.

**Proof of Existence**, an online service that verifies the existence of computer files as of a specific time.

New distribution methods are also becoming available for the insurance industry such as peer-to-peer insurance, parametric insurance and microinsurance following the adoption of blockchain.

The sharing economy and IoT are also set to benefit from blockchains because they involve many collaborating peers. Online voting is another application of the blockchain.

The use of blockchain in libraries is being studied with a grant from the U.S. Institute of Museum and Library Services.

## References and Further Reading for Chapter 2

[http://www.atimes.com/article/internet-firms-try-luck-blockchain-games/](http://www.atimes.com/article/internet-firms-try-luck-blockchain-games/)

[https://www.cnbc.com/2017/12/06/meet-cryptokitties-the-new-digital-beanie-babies-selling-for-100k.html](https://www.cnbc.com/2017/12/06/meet-cryptokitties-the-new-digital-beanie-babies-selling-for-100k.html)

[http://fortune.com/2018/02/13/cryptokitties-ethereum-ios-launch-china-ether/](http://fortune.com/2018/02/13/cryptokitties-ethereum-ios-launch-china-ether/)

[https://web.archive.org/web/20180112143517/http://www.bbc.com/news/technology-42237162](https://web.archive.org/web/20180112143517/http://www.bbc.com/news/technology-42237162)

[https://www.wsj.com/articles/ibm-pushes-blockchain-into-the-supply-chain-1468528824](https://www.wsj.com/articles/ibm-pushes-blockchain-into-the-supply-chain-1468528824)

[https://www.computerworld.com/article/3336036/blockchain/linuxs-hyperledger-to-give-developers-supply-chain-building-blocks.html](https://www.computerworld.com/article/3336036/blockchain/linuxs-hyperledger-to-give-developers-supply-chain-building-blocks.html)

[https://www.hyperledger.org/blog/2019/01/22/announcing-hyperledger-grid-a-new-project-to-help-build-and-deliver-supply-chain-solutions](https://www.hyperledger.org/blog/2019/01/22/announcing-hyperledger-grid-a-new-project-to-help-build-and-deliver-supply-chain-solutions)

[http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8269834](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8269834)

[https://web.archive.org/web/20160923180800/http://fortune.com/2016/09/22/blockchain-music-disruption/](https://web.archive.org/web/20160923180800/http://fortune.com/2016/09/22/blockchain-music-disruption/)

[https://web.archive.org/web/20170410050815/http://www.musicbusinessworldwide.com/ascap-prs-sacem-join-forces-blockchain-copyright-system/](https://web.archive.org/web/20170410050815/http://www.musicbusinessworldwide.com/ascap-prs-sacem-join-forces-blockchain-copyright-system/)

[https://web.archive.org/web/20160422213153/http://www.theguardian.com/music/2015/sep/06/imogen-heap-saviour-of-music-industry](https://web.archive.org/web/20160422213153/http://www.theguardian.com/music/2015/sep/06/imogen-heap-saviour-of-music-industry)

[https://web.archive.org/web/20161114001423/http://www.ey.com/Publication/vwLUAssets/ey-blockchain-reaction-tech-companies-plan-for-critical-mass/\$FILE/ey-blockchain-reaction.pdf](https://web.archive.org/web/20161114001423/http://www.ey.com/Publication/vwLUAssets/ey-blockchain-reaction-tech-companies-plan-for-critical-mass/$FILE/ey-blockchain-reaction.pdf)

[https://web.archive.org/web/20161027082454/https://followmyvote.com/online-voting-platform-faqs/](https://web.archive.org/web/20161027082454/https://followmyvote.com/online-voting-platform-faqs/)

[https://web.archive.org/web/20180205184846/https://www.democracywithoutborders.org/de/4625/reimagining-democracy-what-if-votes-were-a-crypto-currency/](https://web.archive.org/web/20180205184846/https://www.democracywithoutborders.org/de/4625/reimagining-democracy-what-if-votes-were-a-crypto-currency/)

[https://americanlibrariesmagazine.org/2019/03/01/library-blockchain-reaction/](https://americanlibrariesmagazine.org/2019/03/01/library-blockchain-reaction/)

[https://web.archive.org/web/20171207035925/https://www.ibm.com/blockchain/hyperledger.html](https://web.archive.org/web/20171207035925/https://www.ibm.com/blockchain/hyperledger.html)

[https://web.archive.org/web/20170202033844/http://fortune.com/2016/10/04/jp-morgan-chase-blockchain-ethereum-quorum/](https://web.archive.org/web/20170202033844/http://fortune.com/2016/10/04/jp-morgan-chase-blockchain-ethereum-quorum/)

[https://books.google.com/?id=RHJmBgAAQBAJ&q=%22Proof%20of%20Existence%22](https://books.google.com/?id=RHJmBgAAQBAJ&q=%22Proof%20of%20Existence%22)

# 3. Cryptocurrency Basics

A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange that uses strong cryptography to secure financial transactions, control the creation of additional units, and verify the transfer of assets. Cryptocurrencies use decentralized control as opposed to centralized digital currency and central banking systems. The decentralized control of each cryptocurrency works through distributed ledger technology, typically a blockchain, that serves as a public financial transaction database.

![Crypto](./images/crypto.png "Crypto")

Bitcoin, first released as open-source software in 2009, is generally considered the first decentralized cryptocurrency. Since the release of bitcoin, over 6,000 altcoins (alternative variants of bitcoin, or other cryptocurrencies) have been created.

According to Jan Lansky, a cryptocurrency is a system that meets six conditions:

1.  The system does not require a central authority, its state is maintained through distributed consensus.
2.  The system keeps an overview of cryptocurrency units and their ownership.
3.  The system defines whether new cryptocurrency units can be created. If new cryptocurrency units can be created, the system defines the circumstances of their origin and how to determine the ownership of these new units.
4.  Ownership of cryptocurrency units can be proved exclusively cryptographically.
5.  The system allows transactions to be performed in which ownership of the cryptographic units is changed. A transaction statement can only be issued by an entity proving the current ownership of these units.
6.  If two different instructions for changing the ownership of the same cryptographic units are simultaneously entered, the system performs at most one of them.

In 1983, the American cryptographer David Chaum conceived an anonymous cryptographic electronic money called ecash. Later, in 1995, he implemented it through Digicash, an early form of cryptographic electronic payments which required user software in order to withdraw notes from a bank and designate specific encrypted keys before it can be sent to a recipient. This allowed the digital currency to be untraceable by the issuing bank, the government, or any third party.

In 1996, the NSA published a paper entitled How to Make a Mint: the Cryptography of Anonymous Electronic Cash, describing a Cryptocurrency system first publishing it in a MIT mailing list and later in 1997, in The American Law Review (Vol. 46, Issue 4).

In 1998, Wei Dai published a description of "b-money", characterized as an anonymous, distributed electronic cash system. Shortly thereafter, Nick Szabo described bit gold. Like bitcoin and other cryptocurrencies that would follow it, bit gold (not to be confused with the later gold-based exchange, BitGold) was described as an electronic currency system which required users to complete a proof of work function with solutions being cryptographically put together and published. A currency system based on a reusable proof of work was later created by Hal Finney who followed the work of Dai and Szabo.

The first decentralized cryptocurrency, bitcoin, was created in 2009 by pseudonymous developer Satoshi Nakamoto. It used SHA-256, a cryptographic hash function, as its proof-of-work scheme. In April 2011, Namecoin was created as an attempt at forming a decentralized DNS, which would make internet censorship very difficult. Soon after, in October 2011, Litecoin was released. It was the first successful cryptocurrency to use scrypt as its hash function instead of SHA-256. Another notable cryptocurrency, Peercoin was the first to use a proof-of-work/proof-of-stake hybrid.

On 6 August 2014, the UK announced its Treasury had been commissioned to do a study of cryptocurrencies, and what role, if any, they can play in the UK economy. The study was also to report on whether regulation should be considered.

Decentralized cryptocurrency is produced by the entire cryptocurrency system collectively, at a rate which is defined when the system is created and which is publicly known. In centralized banking and economic systems such as the Federal Reserve System, corporate boards or governments control the supply of currency by printing units of fiat money or demanding additions to digital banking ledgers. In the case of decentralized cryptocurrency, companies or governments cannot produce new units, and have not so far provided backing for other firms, banks or corporate entities which hold asset value measured in it. The underlying technical system upon which decentralized cryptocurrencies are based was created by the group or individual known as Satoshi Nakamoto.

As of May 2018, over 1,800 cryptocurrency specifications existed. Within a cryptocurrency system, the safety, integrity and balance of ledgers is maintained by a community of mutually distrustful parties referred to as miners: who use their computers to help validate and timestamp transactions, adding them to the ledger in accordance with a particular timestamping scheme.

Most cryptocurrencies are designed to gradually decrease production of that currency, placing a cap on the total amount of that currency that will ever be in circulation. Compared with ordinary currencies held by financial institutions or kept as cash on hand, cryptocurrencies can be more difficult for seizure by law enforcement. This difficulty is derived from leveraging cryptographic technologies.

Cryptocurrencies use various timestamping schemes to "prove" the validity of transactions added to the blockchain ledger without the need for a trusted third party.

The first timestamping scheme invented was the proof-of-work scheme. The most widely used proof-of-work schemes are based on SHA-256 and scrypt.

Some other hashing algorithms that are used for proof-of-work include CryptoNight, Blake, SHA-3, and X11.

The proof-of-stake is a method of securing a cryptocurrency network and achieving distributed consensus through requesting users to show ownership of a certain amount of currency. It is different from proof-of-work systems that run difficult hashing algorithms to validate electronic transactions. The scheme is largely dependent on the coin, and there's currently no standard form of it. Some cryptocurrencies use a combined proof-of-work/proof-of-stake scheme.

In cryptocurrency networks, mining is a validation of transactions. For this effort, successful miners obtain new cryptocurrency as a reward. The reward decreases transaction fees by creating a complementary incentive to contribute to the processing power of the network. The rate of generating hashes, which validate any transaction, has been increased by the use of specialized machines such as FPGAs and ASICs running complex hashing algorithms like SHA-256 and Scrypt. This arms race for cheaper-yet-efficient machines has been on since the day the first cryptocurrency, bitcoin, was introduced in 2009. With more people venturing into the world of virtual currency, generating hashes for this validation has become far more complex over the years, with miners having to invest large sums of money on employing multiple high performance ASICs. Thus the value of the currency obtained for finding a hash often does not justify the amount of money spent on setting up the machines, the cooling facilities to overcome the enormous amount of heat they produce, and the electricity required to run them.

![Miner](./images/miner.png "Miner")

Some miners pool resources, sharing their processing power over a network to split the reward equally, according to the amount of work they contributed to the probability of finding a block. A "share" is awarded to members of the mining pool who present a valid partial proof-of-work.

As of February 2018, the Chinese Government halted trading of virtual currency, banned initial coin offerings and shut down mining. Some Chinese miners have since relocated to Canada. One company is operating data centers for mining operations at Canadian oil and gas field sites, due to low gas prices. In June 2018, Hydro Quebec proposed to the provincial government to allocate 500 MW to crypto companies for mining. According to a February 2018 report from Fortune, Iceland has become a haven for cryptocurrency miners in part because of its cheap electricity. Prices are contained because nearly all of the country's energy comes from renewable sources, prompting more mining companies to consider opening operations in Iceland.

Transaction fees for cryptocurrency depend mainly on the supply of network capacity at the time, versus the demand from the currency holder for a faster transaction. The currency holder can choose a specific transaction fee, while network entities process transactions in order of highest offered fee to lowest. Cryptocurrency exchanges can simplify the process for currency holders by offering priority alternatives and thereby determine which fee will likely cause the transaction to be processed in the requested time.

For ether, transaction fees differ by computational complexity, bandwidth use, and storage needs, while bitcoin transaction fees differ by transaction size and whether the transaction uses SegWit. In September 2018, the median transaction fee for ether corresponded to $0.017, while for bitcoin it corresponded to $0.55.
Digital currency exchanges allow cryptocurrencies to be exchanged for other cryptocurrencies or for fiat money. They can be brick-and-mortar or strictly online. As a brick-and-mortar business, it exchanges traditional payment methods and digital currencies. As an online business, it exchanges electronically transferred money and digital currencies. Often, the digital currency exchanges operate outside the Western countries to avoid regulation and prosecution. However, they do handle Western fiat currencies and maintain bank accounts in several countries to facilitate deposits in various national currencies. Exchanges may accept credit card payments, wire transfers or other forms of payment in exchange for digital currencies or cryptocurrencies. As of 2018, cryptocurrency and digital exchange regulations in many developed jurisdictions remains unclear as regulators are still considering how to deal with these types of businesses in existence but have not been tested for validity.

The exchanges can send cryptocurrency to a user's personal cryptocurrency wallet. Some can convert digital currency balances into anonymous prepaid cards which can be used to withdraw funds from ATMs worldwide while other digital currencies are backed by real-world commodities such as gold.

The creators of digital currencies are often independent of the digital currency exchange that facilitate trading in the currency. In one type of system, digital currency providers (DCP) are businesses that keep and administer accounts for their customers, but generally do not issue digital currency to those customers directly. Customers buy or sell digital currency from digital currency exchanges, who transfer the digital currency into or out of the customer's DCP account. Some exchanges are subsidiaries of DCP, but many are legally independent businesses.[1] The denomination of funds kept in DCP accounts may be of a real or fictitious currency.

Decentralized exchanges such as Etherdelta, IDEX and HADAX do not store users' funds on the exchange, but instead facilitate peer-to-peer cryptocurrency trading. Decentralized exchanges are resistant to security problems that affect other exchanges, but as of mid 2018 suffer from low trading volumes.

By 2016, several cryptocurrency exchanges operating in the European Union obtained licenses under the EU Payment Services Directive and the EU Electronic Money Directive. The adequacy of such licenses for the operation of a cryptocurrency exchange has not been judicially tested. The European Council and the European Parliament announced that they will issue regulations to impose stricter rules targeting exchange platforms.

In 2018, the US Securities and Exchange Commission maintained that "if a platform offers trading of digital assets that are securities and operates as an "exchange," as defined by the federal securities laws, then the platform must register with the SEC as a national securities exchange or be exempt from registration". The Commodity Futures Trading Commission now permits the trading of cryptocurrency derivatives publicly.

Among the Asian countries, Japan is more forthcoming and regulations mandate the need for a special license from the Financial Services Authority to operate a cryptocurrency exchange. China and Korea remain hostile, with China banning bitcoin miners and freezing bank accounts. While Australia is yet to announce its conclusive regulations on cryptocurrency, it does require its citizens to disclose their digital assets for capitals gains tax.

In order to use and exchange cryptocurrencies, a wallet is needed. A wallet is a device, physical medium, program or a service which stores the public and/or private keys and can be used to track ownership, receive or spend cryptocurrencies. They either store the private key with the user, or the private key is stored remotely and transactions are authorized by a third party. The cryptocurrency itself is not in the wallet. In case of bitcoin and cryptocurrencies derived from it, the cryptocurrency is decentrally stored and maintained in a publicly available ledger called the blockchain.

![Hard Wallet](./images/hard-wallet.png "Hard Wallet")

Multisignature wallets require multiple parties to sign a transaction for any digital money can be spent. Multisignature wallets are designed to have increased security.

With a deterministic wallet a single key can be used to generate an entire tree of key pairs. This single key serves as the root of the tree. The generated mnemonic sentence or word seed is simply a more human-readable way of expressing the key used as the root, as it can be algorithmically converted into the root private key. Those words, in that order, will always generate exactly the same root key.

A word phrase could consist of 24 words like: begin friend black earth beauty praise pride refuse horror believe relief gospel end destroy champion build better awesome. That single root key is not replacing all other private keys, but rather is being used to generate them. All the addresses still have different private keys, but they can all be restored by that single root key. The private keys to every address it has and will ever give out can be recalculated given the root key. That root key, in turn, can be recalculated by feeding in the word seed. The mnemonic sentence is the backup of the wallet. If a wallet supports the same (mnemonic sentence) technique, then the backup can also be restored on another software or hardware wallet.

A mnemonic sentence is considered secure. The BIP-39 standard creates a 512-bit seed from any given mnemonic. The set of possible wallets is 2512. Every passphrase leads to a valid wallet. If the wallet was not previously used it will be empty.

In a non-deterministic wallet, each key is randomly generated on its own accord, and they are not seeded from a common key. Therefore, any backups of the wallet must store each and every single private key used as an address, as well as a buffer of 100 or so future keys that may have already been given out as addresses but not received payments yet.

## References and Further Reading for Chapter 3

[https://web.archive.org/web/20140831001109/http://www.forbes.com/forbes/2011/0509/technology-psilocybin-bitcoins-gavin-andresen-crypto-currency.html](https://web.archive.org/web/20140831001109/http://www.forbes.com/forbes/2011/0509/technology-psilocybin-bitcoins-gavin-andresen-crypto-currency.html)

[https://web.archive.org/web/20171225221125/https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3024330](https://web.archive.org/web/20171225221125/https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3024330)

[https://web.archive.org/web/20171024205446/http://www.heg-fr.ch/EN/School-of-Management/Communication-and-Events/events/Pages/EventViewer.aspx?Event=patrick-schuffel.aspx](https://web.archive.org/web/20171024205446/http://www.heg-fr.ch/EN/School-of-Management/Communication-and-Events/events/Pages/EventViewer.aspx?Event=patrick-schuffel.aspx)

[https://web.archive.org/web/20150912031654/http://www.ibtimes.co.uk/nick-szabo-if-banks-want-benefits-blockchains-they-must-go-permissionless-1518874](https://web.archive.org/web/20150912031654/http://www.ibtimes.co.uk/nick-szabo-if-banks-want-benefits-blockchains-they-must-go-permissionless-1518874)

[https://web.archive.org/web/20151026140555/http://economictimes.indiatimes.com/news/international/business/all-you-need-to-know-about-bitcoin/articleshow/48910867.cms](https://web.archive.org/web/20151026140555/http://economictimes.indiatimes.com/news/international/business/all-you-need-to-know-about-bitcoin/articleshow/48910867.cms)

[https://web.archive.org/web/20160813163512/http://www.trssllc.com/wp-content/uploads/2013/05/White_Paper_Bitcoin_101.pdf](https://web.archive.org/web/20160813163512/http://www.trssllc.com/wp-content/uploads/2013/05/White_Paper_Bitcoin_101.pdf)

[https://web.archive.org/web/20180212005102/http://si-journal.org/index.php/JSI/article/viewFile/335/325](https://web.archive.org/web/20180212005102/http://si-journal.org/index.php/JSI/article/viewFile/335/325)

[https://web.archive.org/web/20141218034712/http://www.hit.bme.hu/~buttyan/courses/BMEVIHIM219/2009/Chaum.BlindSigForPayment.1982.PDF](https://web.archive.org/web/20141218034712/http://www.hit.bme.hu/~buttyan/courses/BMEVIHIM219/2009/Chaum.BlindSigForPayment.1982.PDF)

[https://web.archive.org/web/20110903023027/http://blog.koehntopp.de/uploads/chaum_fiat_naor_ecash.pdf](https://web.archive.org/web/20110903023027/http://blog.koehntopp.de/uploads/chaum_fiat_naor_ecash.pdf)

[https://web.archive.org/web/20170830214226/https://www.forbes.com/forbes/1999/1101/6411390a.html](https://web.archive.org/web/20170830214226/https://www.forbes.com/forbes/1999/1101/6411390a.html)

[https://web.archive.org/web/20171026230354/http://groups.csail.mit.edu/mac/classes/6.805/articles/money/nsamint/nsamint.htm](https://web.archive.org/web/20171026230354/http://groups.csail.mit.edu/mac/classes/6.805/articles/money/nsamint/nsamint.htm)

[https://web.archive.org/web/20180112043050/http://digitalcommons.wcl.american.edu/aulr/vol46/iss4/6/](https://web.archive.org/web/20180112043050/http://digitalcommons.wcl.american.edu/aulr/vol46/iss4/6/)

[https://www.webcitation.org/62ArKBIqT?url=http://www.weidai.com/bmoney.txt](https://www.webcitation.org/62ArKBIqT?url=http://www.weidai.com/bmoney.txt)

[https://web.archive.org/web/20120604063739/http://spectrum.ieee.org/computing/software/bitcoin-the-cryptoanarchists-answer-to-cash/0](https://web.archive.org/web/20120604063739/http://spectrum.ieee.org/computing/software/bitcoin-the-cryptoanarchists-answer-to-cash/0)

[https://web.archive.org/web/20130921060724/http://mercatus.org/sites/default/files/Brito_BitcoinPrimer.pdf](https://web.archive.org/web/20130921060724/http://mercatus.org/sites/default/files/Brito_BitcoinPrimer.pdf)

[https://web.archive.org/web/20141003104345/http://www.pcworld.com/article/2039184/bitcoin-developer-talks-regulation-open-source-and-the-elusive-satoshi-nakamoto.html](https://web.archive.org/web/20141003104345/http://www.pcworld.com/article/2039184/bitcoin-developer-talks-regulation-open-source-and-the-elusive-satoshi-nakamoto.html)

[https://web.archive.org/web/20140116040533/http://arstechnica.com/business/2013/05/wary-of-bitcoin-a-guide-to-some-other-cryptocurrencies/](https://web.archive.org/web/20140116040533/http://arstechnica.com/business/2013/05/wary-of-bitcoin-a-guide-to-some-other-cryptocurrencies/)

[https://web.archive.org/web/20141110202812/http://www.theuknews.com/index.php/sid/224504231](https://web.archive.org/web/20141110202812/http://www.theuknews.com/index.php/sid/224504231)

[https://web.archive.org/web/20160703000844/http://www.economist.com/news/briefing/21677228-technology-behind-bitcoin-lets-people-who-do-not-know-or-trust-each-other-build-dependable](https://web.archive.org/web/20160703000844/http://www.economist.com/news/briefing/21677228-technology-behind-bitcoin-lets-people-who-do-not-know-or-trust-each-other-build-dependable)

[https://web.archive.org/web/20180515183618/https://www.ft.com/content/29dcb760-5787-11e8-b8b2-d6ceb45fa9d0](https://web.archive.org/web/20180515183618/https://www.ft.com/content/29dcb760-5787-11e8-b8b2-d6ceb45fa9d0)

[https://web.archive.org/web/20130927084451/http://www.americanbanker.com/bankthink/how-cryptocurrencies-could-upend-banks-monetary-role-1057597-1.html?zkPrintable=1&nopagination=1](https://web.archive.org/web/20130927084451/http://www.americanbanker.com/bankthink/how-cryptocurrencies-could-upend-banks-monetary-role-1057597-1.html?zkPrintable=1&nopagination=1)

[https://web.archive.org/web/20180123061505/https://www.theguardian.com/technology/2018/jan/17/bitcoin-electricity-usage-huge-climate-cryptocurrency](https://web.archive.org/web/20180123061505/https://www.theguardian.com/technology/2018/jan/17/bitcoin-electricity-usage-huge-climate-cryptocurrency)

[https://web.archive.org/web/20180304090651/https://www.bloomberg.com/news/articles/2018-02-02/china-crypto-exodus-leads-to-cheap-clean-power-in-chilly-canada](https://web.archive.org/web/20180304090651/https://www.bloomberg.com/news/articles/2018-02-02/china-crypto-exodus-leads-to-cheap-clean-power-in-chilly-canada)

[https://web.archive.org/web/20180130091353/http://www.worldoil.com/news/2018/1/26/cryptocurrency-mining-operation-launched-by-iron-bridge-resources](https://web.archive.org/web/20180130091353/http://www.worldoil.com/news/2018/1/26/cryptocurrency-mining-operation-launched-by-iron-bridge-resources)

[https://marketexclusive.com/bitcoin-major-currencies-today-crypto-currency-daily-roundup-june-25/2018/06/](https://marketexclusive.com/bitcoin-major-currencies-today-crypto-currency-daily-roundup-june-25/2018/06/)

[https://web.archive.org/web/20180420081336/http://fortune.com/2018/02/13/iceland-bitcoin-mining-electricity/](https://web.archive.org/web/20180420081336/http://fortune.com/2018/02/13/iceland-bitcoin-mining-electricity/)

[https://web.archive.org/web/20180320170609/https://www.bloomberg.com/news/articles/2018-03-16/bitcoin-mining-banned-for-first-time-in-upstate-new-york-town](https://web.archive.org/web/20180320170609/https://www.bloomberg.com/news/articles/2018-03-16/bitcoin-mining-banned-for-first-time-in-upstate-new-york-town)

[https://web.archive.org/web/20181026064457/https://ethgasstation.info/](https://web.archive.org/web/20181026064457/https://ethgasstation.info/)

[https://web.archive.org/web/20140211235106/http://www.secp.gov.pk/IACCD/pub_iaccd/AML/MONEY%20LAUNDERING%20%26%20TERRORIST%20FINANCING%20VULNERABILITIES%20OF%20COMMERCIAL%20WEBSITES%20AND%20INTERNET%20PAYMENT%20SYSTEMS.pdf](https://web.archive.org/web/20140211235106/http://www.secp.gov.pk/IACCD/pub_iaccd/AML/MONEY%20LAUNDERING%20%26%20TERRORIST%20FINANCING%20VULNERABILITIES%20OF%20COMMERCIAL%20WEBSITES%20AND%20INTERNET%20PAYMENT%20SYSTEMS.pdf)

[https://www.justice.gov/archive/ndic/pubs28/28675/sub.htm](https://www.justice.gov/archive/ndic/pubs28/28675/sub.htm)

_Sood, Aditya K; Enbody, Richard J; Bansal, Rohit (2013). "Cybercrime: Dissecting the State of Underground Enterprise". IEEE Internet Computing (1). IEEE Computer Society. pp. 60–68. doi:10.1109/MIC.2012.61._

[https://books.google.com/books?id=cVLUdo4JQv4C&pg=PP2802](https://books.google.com/books?id=cVLUdo4JQv4C&pg=PP2802)

[https://books.google.com/books?id=pX5uOpcXkdgC&pg=PA218](https://books.google.com/books?id=pX5uOpcXkdgC&pg=PA218)

[https://www.wsj.com/articles/why-cryptocurrency-exchange-hacks-keep-happening-1531656000](https://www.wsj.com/articles/why-cryptocurrency-exchange-hacks-keep-happening-1531656000)

[https://arstechnica.com/information-technology/2016/04/bitcoin-firm-circle-electronic-money-licence-uk-barclays/](https://arstechnica.com/information-technology/2016/04/bitcoin-firm-circle-electronic-money-licence-uk-barclays/)

[https://www.sec.gov/news/public-statement/enforcement-tm-statement-potentially-unlawful-online-platforms-trading](https://www.sec.gov/news/public-statement/enforcement-tm-statement-potentially-unlawful-online-platforms-trading)

[https://www.cnbc.com/2018/03/07/the-sec-made-it-clearer-that-securities-laws-apply-to-cryptocurrencies.html](https://www.cnbc.com/2018/03/07/the-sec-made-it-clearer-that-securities-laws-apply-to-cryptocurrencies.html)

[https://www.cnbc.com/2018/03/23/japanese-regulator-warns-major-cryptocurrency-exchange-for-operating-without-a-license-bitcoin-falls.html](https://www.cnbc.com/2018/03/23/japanese-regulator-warns-major-cryptocurrency-exchange-for-operating-without-a-license-bitcoin-falls.html)

[https://www.forbes.com/sites/sarahsu/2018/01/15/chinas-shutdown-of-bitcoin-miners-isnt-just-about-electricity/#41b7ee23369b](https://www.forbes.com/sites/sarahsu/2018/01/15/chinas-shutdown-of-bitcoin-miners-isnt-just-about-electricity/#41b7ee23369b)

[https://www.yicaiglobal.com/news/china%E2%80%99s-regulators-freeze-multiple-bitcoin-otc-accounts-latest-crackdown-cryptocurrency](https://www.yicaiglobal.com/news/china%E2%80%99s-regulators-freeze-multiple-bitcoin-otc-accounts-latest-crackdown-cryptocurrency)

[https://www.ato.gov.au/misc/downloads/pdf/qc42159.pdf](https://www.ato.gov.au/misc/downloads/pdf/qc42159.pdf)

[https://finance.yahoo.com/news/send-bitcoin-hardware-wallet-140141385.html](https://finance.yahoo.com/news/send-bitcoin-hardware-wallet-140141385.html)

[https://books.google.com/?id=MpwnDwAAQBAJ&pg=PA93#v=onepage&q&f=false](https://books.google.com/?id=MpwnDwAAQBAJ&pg=PA93#v=onepage&q&f=false)

[https://www.dailydot.com/debug/bitcoin-wallets-cryptocurrency-hardware/](https://www.dailydot.com/debug/bitcoin-wallets-cryptocurrency-hardware/)

[https://www.wired.com/story/how-to-keep-bitcoin-safe-and-secure/](https://www.wired.com/story/how-to-keep-bitcoin-safe-and-secure/)

[https://eprint.iacr.org/2014/998.pdf](https://eprint.iacr.org/2014/998.pdf)

# 4. Regulation of Cryptocurrencies

While cryptocurrencies are digital currencies that are managed through advanced encryption techniques, many governments have taken a cautious approach toward them, fearing their lack of central control and the effects they could have on financial security.[81] Regulators in several countries have warned against cryptocurrency and some have taken concrete regulatory measures to dissuade users. Additionally, many banks do not offer services for cryptocurrencies and can refuse to offer services to virtual-currency companies.

Gareth Murphy, a senior central banking officer has stated "widespread use [of cryptocurrency] would also make it more difficult for statistical agencies to gather data on economic activity, which are used by governments to steer the economy". He cautioned that virtual currencies pose a new challenge to central banks' control over the important functions of monetary and exchange rate policy. While traditional financial products have strong consumer protections in place, there is no intermediary with the power to limit consumer losses if bitcoins are lost or stolen. One of the features cryptocurrency lacks in comparison to credit cards, for example, is consumer protection against fraud, such as chargebacks.

There are also purely technical elements to consider. For example, technological advancement in cryptocurrencies such as bitcoin result in high up-front costs to miners in the form of specialized hardware and software. Cryptocurrency transactions are normally irreversible after a number of blocks confirm the transaction. Additionally, cryptocurrency private keys can be permanently lost from local storage due to malware, data loss or the destruction of the physical media. This prevents the cryptocurrency from being spent, resulting in its effective removal from the markets.

The legal status of cryptocurrencies varies substantially from country to country and is still undefined or changing in many of them. While some countries have explicitly allowed their use and trade, others have banned or restricted it.

According to the Library of Congress, an "absolute ban" on trading or using cryptocurrencies applies in eight countries: Algeria, Bolivia, Egypt, Iraq, Morocco, Nepal, Pakistan, and the United Arab Emirates. An "implicit ban" applies in another 15 countries, which include Bahrain, Bangladesh, China, Colombia, the Dominican Republic, Indonesia, Iran, Kuwait, Lesotho, Lithuania, Macau, Oman, Qatar, Saudi Arabia and Taiwan.
In the United States and Canada, state and provincial securities regulators, coordinated through the North American Securities Administrators Association, are investigating "bitcoin scams" and ICOs in 40 jurisdictions.

Various government agencies, departments, and courts have classified bitcoin differently. China Central Bank banned the handling of bitcoins by financial institutions in China in early 2014.

In Russia, though cryptocurrencies are legal, it is illegal to actually purchase goods with any currency other than the Russian ruble. Regulations and bans that apply to bitcoin probably extend to similar cryptocurrency systems.

Cryptocurrencies are a potential tool to evade economic sanctions for example against Russia, Iran, or Venezuela. Russia also secretly supported Venezuela with the creation of the petro (El Petro), a national cryptocurrency initiated by the Maduro government to obtain valuable oil revenues by circumventing US sanctions.

In August 2018, the Bank of Thailand announced its plans to create its own cryptocurrency, the Central Bank Digital Currency (CBDC). UNICEF accepts cryptocurrency donations.

On 25 March 2014, the United States Internal Revenue Service (IRS) ruled that bitcoin will be treated as property for tax purposes. This means bitcoin will be subject to capital gains tax.[62] In a paper published by researchers from Oxford and Warwick, it was shown that bitcoin has some characteristics more like the precious metals market than traditional currencies, hence in agreement with the IRS decision even if based on different reasons. In July 2019, the IRS started sending letters to cryptocurrency owners warning them to amend their returns and pay taxes.

As the popularity of and demand for online currencies has increased since the inception of bitcoin in 2009, so have concerns that such an unregulated person to person global economy that cryptocurrencies offer may become a threat to society. Concerns abound that altcoins may become tools for anonymous web criminals.

Transactions that occur through the use and exchange of these altcoins are independent from formal banking systems, and therefore can make tax evasion simpler for individuals. Since charting taxable income is based upon what a recipient reports to the revenue service, it becomes extremely difficult to account for transactions made using existing cryptocurrencies, a mode of exchange that is complex and difficult to track.

Systems of anonymity that most cryptocurrencies offer can also serve as a simpler means to launder money. Rather than laundering money through an intricate net of financial actors and offshore bank accounts, laundering money through altcoins can be achieved through anonymous transactions.

In February 2014 the world's largest bitcoin exchange, Mt. Gox, declared bankruptcy. The company stated that it had lost nearly $473 million of their customers' bitcoins likely due to theft. This was equivalent to approximately 750,000 bitcoins, or about 7% of all the bitcoins in existence. The price of a bitcoin fell from a high of about $1,160 in December to under \$400 in February.

Two members of the Silk Road Task Force—a multi-agency federal task force that carried out the U.S. investigation of Silk Road—seized bitcoins for their own use in the course of the investigation. DEA agent Carl Mark Force IV, who attempted to extort Silk Road founder Ross Ulbricht ("Dread Pirate Roberts"), pleaded guilty to money laundering, obstruction of justice, and extortion under color of official right, and was sentenced to 6.5 years in federal prison. U.S. Secret Service agent Shaun Bridges pleaded guilty to crimes relating to his diversion of \$800,000 worth of bitcoins to his personal account during the investigation, and also separately pleaded guilty to money laundering in connection with another cryptocurrency theft; he was sentenced to nearly eight years in federal prison.

Homero Josh Garza, who founded the cryptocurrency startups GAW Miners and ZenMiner in 2014, acknowledged in a plea agreement that the companies were part of a pyramid scheme, and pleaded guilty to wire fraud in 2015. The U.S. Securities and Exchange Commission separately brought a civil enforcement action against Garza, who was eventually ordered to pay a judgment of $9.1 million plus $700,000 in interest. The SEC's complaint stated that Garza, through his companies, had fraudulently sold "investment contracts representing shares in the profits they claimed would be generated" from mining.

On 21 November 2017, the Tether cryptocurrency announced they were hacked, losing \$31 million in USDT from their primary wallet. The company has 'tagged' the stolen currency, hoping to 'lock' them in the hacker's wallet (making them unspendable). Tether indicates that it is building a new core for its primary wallet in response to the attack in order to prevent the stolen coins from being used.

In May 2018, Bitcoin Gold (and two other cryptocurrencies) were hit by a successful 51% hashing attack by an unknown actor, in which exchanges lost estimated $18m. In June 2018, Korean exchange Coinrail was hacked, losing US$37 million worth of altcoin. Fear surrounding the hack was blamed for a $42 billion cryptocurrency market selloff. On 9 July 2018 the exchange Bancor had $23.5 million in cryptocurrency stolen.

The French regulator Autorité des marchés financiers (AMF) lists 15 websites of companies that solicit investment in cryptocurrency without being authorised to do so in France.

Properties of cryptocurrencies gave them popularity in applications such as a safe haven in banking crises and means of payment, which also led to the cryptocurrency use in controversial settings in the form of online black markets, such as Silk Road. The original Silk Road was shut down in October 2013 and there have been two more versions in use since then. In the year following the initial shutdown of Silk Road, the number of prominent dark markets increased from four to twelve, while the amount of drug listings increased from 18,000 to 32,000.

Darknet markets present challenges in regard to legality. Cryptocurrency used in dark markets are not clearly or legally classified in almost all parts of the world. In the U.S., bitcoins are labelled as "virtual assets". This type of ambiguous classification puts pressure on law enforcement agencies around the world to adapt to the shifting drug trade of dark markets.

## References and Further Reading for Chapter 4

[https://web.archive.org/web/20140815121121/http://www.meetup.com/BitcoinDC/events/176047462/?oc=evam](https://web.archive.org/web/20140815121121/http://www.meetup.com/BitcoinDC/events/176047462/?oc=evam)

[https://web.archive.org/web/20131229064125/http://www.bloomberg.com/news/2013-12-17/bitcoin-rules-drafted-in-denmark-as-regulator-warns-against-use.html](https://web.archive.org/web/20131229064125/http://www.bloomberg.com/news/2013-12-17/bitcoin-rules-drafted-in-denmark-as-regulator-warns-against-use.html)

[https://web.archive.org/web/20151119021729/http://www.wsj.com/news/articles/SB10001424052702304202204579252850121034702](https://web.archive.org/web/20151119021729/http://www.wsj.com/news/articles/SB10001424052702304202204579252850121034702)

[https://web.archive.org/web/20160304081304/http://www.rte.ie/news/business/2014/0703/628309-bitfin-conference/](https://web.archive.org/web/20160304081304/http://www.rte.ie/news/business/2014/0703/628309-bitfin-conference/)

[https://web.archive.org/web/20170823075106/https://www.forbes.com/sites/timothylee/2013/04/03/four-reason-you-shouldnt-buy-bitcoins/](https://web.archive.org/web/20170823075106/https://www.forbes.com/sites/timothylee/2013/04/03/four-reason-you-shouldnt-buy-bitcoins/)

[https://web.archive.org/web/20140311220151/http://letstalkbitcoin.com/experiments-in-cryptocurrency-sustainability/](https://web.archive.org/web/20140311220151/http://letstalkbitcoin.com/experiments-in-cryptocurrency-sustainability/)

[https://web.archive.org/web/20140505181855/http://theweek.com/article/index/242753/want-to-make-money-off-bitcoin-mining-hint-dont-mine](https://web.archive.org/web/20140505181855/http://theweek.com/article/index/242753/want-to-make-money-off-bitcoin-mining-hint-dont-mine)

[https://web.archive.org/web/20140712235229/http://c4ss.org/content/25638](https://web.archive.org/web/20140712235229/http://c4ss.org/content/25638)

[https://web.archive.org/web/20180319214207/https://www.cnbc.com/2017/04/12/bitcoin-price-rises-japan-russia-regulation.html](https://web.archive.org/web/20180319214207/https://www.cnbc.com/2017/04/12/bitcoin-price-rises-japan-russia-regulation.html)

[https://web.archive.org/web/20180814071440/https://www.loc.gov/law/help/cryptocurrency/cryptocurrency-world-survey.pdf](https://web.archive.org/web/20180814071440/https://www.loc.gov/law/help/cryptocurrency/cryptocurrency-world-survey.pdf)

[https://web.archive.org/web/20180527081727/https://www.washingtonpost.com/news/the-switch/wp/2018/05/21/state-regulators-unveil-nationwide-crackdown-on-suspicious-cryptocurrency-investment-schemes/](https://web.archive.org/web/20180527081727/https://www.washingtonpost.com/news/the-switch/wp/2018/05/21/state-regulators-unveil-nationwide-crackdown-on-suspicious-cryptocurrency-investment-schemes/)

[https://web.archive.org/web/20170916060759/https://www.forbes.com/sites/kashmirhill/2014/01/31/bitcoins-legality-around-the-world/](https://web.archive.org/web/20170916060759/https://www.forbes.com/sites/kashmirhill/2014/01/31/bitcoins-legality-around-the-world/)

_Tasca, Paolo (7 September 2015). "Digital Currencies: Principles, Trends, Opportunities, and Risks". Social Science Research Network. SSRN 2657598._

[https://web.archive.org/web/20180827142601/http://www.atimes.com/article/bank-of-thailand-to-launch-its-own-crypto-currency/amp/](https://web.archive.org/web/20180827142601/http://www.atimes.com/article/bank-of-thailand-to-launch-its-own-crypto-currency/amp/)

[https://www.ibtimes.com/unicef-blockchain-fund-first-un-organization-accept-cryptocurrency-donations-2845332](https://www.ibtimes.com/unicef-blockchain-fund-first-un-organization-accept-cryptocurrency-donations-2845332)

[https://web.archive.org/web/20180401084746/https://www.timesofisrael.com/european-union-bans-binary-options-strictly-regulates-cfds/](https://web.archive.org/web/20180401084746/https://www.timesofisrael.com/european-union-bans-binary-options-strictly-regulates-cfds/)

[https://web.archive.org/web/20160601151330/https://www.theguardian.com/technology/2014/mar/25/bitcoin-property-currency-irs-rules](https://web.archive.org/web/20160601151330/https://www.theguardian.com/technology/2014/mar/25/bitcoin-property-currency-irs-rules)

[https://web.archive.org/web/20170508200903/https://arxiv.org/pdf/1411.1924.pdf](https://web.archive.org/web/20170508200903/https://arxiv.org/pdf/1411.1924.pdf)

_Iwamura, Mitsuru; Kitamura, Yukinobu; Matsumoto, Tsutomu (28 February 2014). "Is Bitcoin the Only Cryptocurrency in the Town? Economics of Cryptocurrency and Friedrich A. Hayek". doi:10.2139/ssrn.2405790. hdl:10086/26493. SSRN 2405790._

_ALI, S, T; CLARKE, D; MCCORRY, P; Bitcoin: Perils of an Unregulated Global P2P Currency [By S. T Ali, D. Clarke, P. McCorry Newcastle upon Tyne: Newcastle University: Computing Science, 2015. (Newcastle University, Computing Science, Technical Report Series, No. CS-TR-1470)_

[https://web.archive.org/web/20150112234804/http://www.bloomberg.com/news/2014-02-28/mt-gox-exchange-files-for-bankruptcy.html](https://web.archive.org/web/20150112234804/http://www.bloomberg.com/news/2014-02-28/mt-gox-exchange-files-for-bankruptcy.html)

[https://web.archive.org/web/20171229113002/https://motherboard.vice.com/en_us/article/8q845p/dea-agent-who-faked-a-murder-and-took-bitcoins-from-silk-road-explains-himself](https://web.archive.org/web/20171229113002/https://motherboard.vice.com/en_us/article/8q845p/dea-agent-who-faked-a-murder-and-took-bitcoins-from-silk-road-explains-himself)

[https://web.archive.org/web/20171229112652/https://www.reuters.com/article/us-usa-cyber-silkroad/ex-agent-in-silk-road-probe-gets-more-prison-time-for-bitcoin-theft-idUSKBN1D804H](https://web.archive.org/web/20171229112652/https://www.reuters.com/article/us-usa-cyber-silkroad/ex-agent-in-silk-road-probe-gets-more-prison-time-for-bitcoin-theft-idUSKBN1D804H)

[https://web.archive.org/web/20171229112405/https://arstechnica.com/tech-policy/2017/10/bitcoin-fraudster-hit-with-9-1m-civil-judgment-on-top-of-criminal-guilty-plea/](https://web.archive.org/web/20171229112405/https://arstechnica.com/tech-policy/2017/10/bitcoin-fraudster-hit-with-9-1m-civil-judgment-on-top-of-criminal-guilty-plea/)

[https://web.archive.org/web/20171121105158/https://techcrunch.com/2017/11/20/tether-claims-a-hacker-stole-31m/](https://web.archive.org/web/20171121105158/https://techcrunch.com/2017/11/20/tether-claims-a-hacker-stole-31m/)

[https://web.archive.org/web/20180612005144/https://www.bloomberg.com/news/articles/2018-06-10/bitcoin-tumbles-most-in-two-weeks-amid-south-korea-exchange-hack](https://web.archive.org/web/20180612005144/https://www.bloomberg.com/news/articles/2018-06-10/bitcoin-tumbles-most-in-two-weeks-amid-south-korea-exchange-hack)

[https://web.archive.org/web/20180710013104/http://fortune.com/2018/07/09/bancor-hack/](https://web.archive.org/web/20180710013104/http://fortune.com/2018/07/09/bancor-hack/)

[https://web.archive.org/web/20180511013141/http://www.amf-france.org/en_US/Actualites/Communiques-de-presse/AMF/annee-2018?docId=workspace%3A%2F%2FSpacesStore%2F3f85fe88-fc1f-45b8-a55e-0bc5aeb298ce](https://web.archive.org/web/20180511013141/http://www.amf-france.org/en_US/Actualites/Communiques-de-presse/AMF/annee-2018?docId=workspace%3A%2F%2FSpacesStore%2F3f85fe88-fc1f-45b8-a55e-0bc5aeb298ce)

[https://web.archive.org/web/20151222084405/http://gjis.journals.yorku.ca/index.php/gjis/article/view/38935](https://web.archive.org/web/20151222084405/http://gjis.journals.yorku.ca/index.php/gjis/article/view/38935)

# 5. Deep Look at Bitcoin

On 18 August 2008, the domain name bitcoin.org was registered. Later that year, on 31 October, a link to a paper authored by Satoshi Nakamoto titled Bitcoin: A Peer-to-Peer Electronic Cash System was posted to a cryptography mailing list. This paper detailed methods of using a peer-to-peer network to generate what was described as "a system for electronic transactions without relying on trust". On 3 January 2009, the bitcoin network came into existence with Satoshi Nakamoto mining the genesis block of bitcoin (block number 0), which had a reward of 50 bitcoins. Embedded in the coinbase of this block was the text:

The Times 03/Jan/2009 Chancellor on brink of second bailout for banks.

The text refers to a headline in The Times published on 3 January 2009. This note has been interpreted as both a timestamp of the genesis date and a derisive comment on the instability caused by fractional-reserve banking.

The first open source bitcoin client was released on 9 January 2009, hosted at SourceForge. One of the first supporters, adopters, contributors to bitcoin and receiver of the first bitcoin transaction was programmer Hal Finney. Finney downloaded the bitcoin software the day it was released, and received 10 bitcoins from Nakamoto in the world's first bitcoin transaction on 12 January 2009. Other early supporters were Wei Dai, creator of bitcoin predecessor b-money, and Nick Szabo, creator of bitcoin predecessor bit gold.

In the early days, Nakamoto is estimated to have mined 1 million bitcoins. Before disappearing from any involvement in bitcoin, Nakamoto in a sense handed over the reins to developer Gavin Andresen, who then became the bitcoin lead developer at the Bitcoin Foundation, the 'anarchic' bitcoin community's closest thing to an official public face.

The value of the first bitcoin transactions were negotiated by individuals on the bitcoin forum with one notable transaction of 10,000 BTC used to indirectly purchase two pizzas delivered by Papa John's.

On 6 August 2010, a major vulnerability in the bitcoin protocol was spotted. Transactions weren't properly verified before they were included in the transaction log or blockchain, which let users bypass bitcoin's economic restrictions and create an indefinite number of bitcoins. On 15 August, the vulnerability was exploited; over 184 billion bitcoins were generated in a transaction, and sent to two addresses on the network. Within hours, the transaction was spotted and erased from the transaction log after the bug was fixed and the network forked to an updated version of the bitcoin protocol. This was the only major security flaw found and exploited in bitcoin's history.

The unit of account of the bitcoin system is a bitcoin. Ticker symbols used to represent bitcoin are BTC[b] and XBT. Its Unicode character is ₿. Small amounts of bitcoin used as alternative units are millibitcoin (mBTC), and satoshi (sat). Named in homage to bitcoin's creator, a satoshi is the smallest amount within bitcoin representing 0.00000001 bitcoins, one hundred millionth of a bitcoin. A millibitcoin equals 0.001 bitcoins; one thousandth of a bitcoin or 100,000 satoshis.

The bitcoin blockchain is a public ledger that records bitcoin transactions. It is implemented as a chain of blocks, each block containing a hash of the previous block up to the genesis block of the chain. A network of communicating nodes running bitcoin software maintains the blockchain. Transactions of the form payer X sends Y bitcoins to payee Z are broadcast to this network using readily available software applications.

Network nodes can validate transactions, add them to their copy of the ledger, and then broadcast these ledger additions to other nodes. To achieve independent verification of the chain of ownership each network node stores its own copy of the blockchain. About every 10 minutes, a new group of accepted transactions, called a block, is created, added to the blockchain, and quickly published to all nodes, without requiring central oversight. This allows bitcoin software to determine when a particular bitcoin was spent, which is needed to prevent double-spending. A conventional ledger records the transfers of actual bills or promissory notes that exist apart from it, but the blockchain is the only place that bitcoins can be said to exist in the form of unspent outputs of transactions.

Transactions are defined using a Forth-like scripting language. Transactions consist of one or more inputs and one or more outputs. When a user sends bitcoins, the user designates each address and the amount of bitcoin being sent to that address in an output. To prevent double spending, each input must refer to a previous unspent output in the blockchain. The use of multiple inputs corresponds to the use of multiple coins in a cash transaction. Since transactions can have multiple outputs, users can send bitcoins to multiple recipients in one transaction. As in a cash transaction, the sum of inputs (coins used to pay) can exceed the intended sum of payments. In such a case, an additional output is used, returning the change back to the payer. Any input satoshis not accounted for in the transaction outputs become the transaction fee.

Though transaction fees are optional, miners can choose which transactions to process and prioritize those that pay higher fees. Miners may choose transactions based on the fee paid relative to their storage size, not the absolute amount of money paid as a fee. These fees are generally measured in satoshis per byte (sat/b). The size of transactions is dependent on the number of inputs used to create the transaction, and the number of outputs.

In the blockchain, bitcoins are registered to bitcoin addresses. Creating a bitcoin address requires nothing more than picking a random valid private key and computing the corresponding bitcoin address. This computation can be done in a split second. But the reverse, computing the private key of a given bitcoin address, is mathematically unfeasible. Users can tell others or make public a bitcoin address without compromising its corresponding private key. Moreover, the number of valid private keys is so vast that it is extremely unlikely someone will compute a key-pair that is already in use and has funds. The vast number of valid private keys makes it unfeasible that brute force could be used to compromise a private key. To be able to spend their bitcoins, the owner must know the corresponding private key and digitally sign the transaction. The network verifies the signature using the public key; the private key is never revealed.

If the private key is lost, the bitcoin network will not recognize any other evidence of ownership; the coins are then unusable, and effectively lost. For example, in 2013 one user claimed to have lost 7,500 bitcoins, worth $7.5 million at the time, when he accidentally discarded a hard drive containing his private key. About 20% of all bitcoins are believed to be lost. They would have a market value of about $20 billion at July 2018 prices.

To ensure the security of bitcoins, the private key must be kept secret. If the private key is revealed to a third party, e.g. through a data breach, the third party can use it to steal any associated bitcoins. As of December 2017, around 980,000 bitcoins have been stolen from cryptocurrency exchanges. Regarding ownership distribution, as of 16 March 2018, 0.5% of bitcoin wallets own 87% of all bitcoins ever mined.

Mining is a record-keeping service done through the use of computer processing power. Miners keep the blockchain consistent, complete, and unalterable by repeatedly grouping newly broadcast transactions into a block, which is then broadcast to the network and verified by recipient nodes. Each block contains a SHA-256 cryptographic hash of the previous block, thus linking it to the previous block and giving the blockchain its name.

To be accepted by the rest of the network, a new block must contain a proof-of-work (PoW). The system used is based on Adam Back's 1997 anti-spam scheme, Hashcash. The PoW requires miners to find a number called a nonce, such that when the block content is hashed along with the nonce, the result is numerically smaller than the network's difficulty target. This proof is easy for any node in the network to verify, but extremely time-consuming to generate, as for a secure cryptographic hash, miners must try many different nonce values (usually the sequence of tested values is the ascending natural numbers: 0, 1, 2, 3, ...) before meeting the difficulty target.

Every 2,016 blocks (approximately 14 days at roughly 10 min per block), the difficulty target is adjusted based on the network's recent performance, with the aim of keeping the average time between new blocks at ten minutes. In this way the system automatically adapts to the total amount of mining power on the network. Between 1 March 2014 and 1 March 2015, the average number of nonces miners had to try before creating a new block increased from 16.4 quintillion to 200.5 quintillion.

The proof-of-work system, alongside the chaining of blocks, makes modifications of the blockchain extremely hard, as an attacker must modify all subsequent blocks in order for the modifications of one block to be accepted. As new blocks are mined all the time, the difficulty of modifying a block increases as time passes and the number of subsequent blocks (also called confirmations of the given block) increases.

The successful miner finding the new block is allowed by the rest of the network to reward themselves with newly created bitcoins and transaction fees. As of 9 July 2016, the reward amounted to 12.5 newly created bitcoins per block added to the blockchain, plus any transaction fees from payments processed by the block. To claim the reward, a special transaction called a coinbase is included with the processed payments. All bitcoins in existence have been created in such coinbase transactions. The bitcoin protocol specifies that the reward for adding a block will be halved every 210,000 blocks (approximately every four years). Eventually, the reward will decrease to zero, and the limit of 21 million bitcoins[g] will be reached c. 2140; the record keeping will then be rewarded solely by transaction fees.

In other words, Nakamoto set a monetary policy based on artificial scarcity at bitcoin's inception that the total number of bitcoins could never exceed 21 million. New bitcoins are created roughly every ten minutes and the rate at which they are generated drops by half about every four years until all will be in circulation.

Bitcoin is decentralized and does not have any central authority. The bitcoin network, including the ledger is peer-to-peer. The ledger itself is also public and is maintained by a network of equally privileged miners. Anybody can become a miner.

The additions to the ledger are maintained through competition. Until a new block is added to the ledger, it is not known which miner will create the block. The issuance of bitcoins is a reward for the creation of a new block. Anybody can create a new bitcoin address (a bitcoin counterpart of a bank account) without needing any approval. Anybody can send a transaction to the network without needing any approval; the network merely confirms that the transaction is legitimate.

Researchers have pointed out at a "trend towards centralization". Although bitcoin can be sent directly from user to user, in practice intermediaries are widely used. Bitcoin miners join large mining pools to minimize the variance of their income. Because transactions on the network are confirmed by miners, decentralization of the network requires that no single miner or mining pool obtains 51% of the hashing power, which would allow them to double-spend coins, prevent certain transactions from being verified and prevent other miners from earning income.[125] As of 2013 just six mining pools controlled 75% of overall bitcoin hashing power. In 2014 mining pool Ghash.io obtained 51% hashing power which raised significant controversies about the safety of the network. The pool has voluntarily capped their hashing power at 39.99% and requested other pools to act responsibly for the benefit of the whole network. c. 2017 over 70% of the hashing power and 90% of transactions were operating from China.

According to researchers, other parts of the ecosystem are also "controlled by a small set of entities", notably the maintenance of the client software, online wallets and simplified payment verification (SPV) clients.

Satoshi Nakamoto stated in his white paper that: "The root problem with conventional currencies is all the trust that's required to make it work. The central bank must be trusted not to debase the currency, but the history of fiat currencies is full of breaches of that trust."

According to the European Central Bank, the decentralization of money offered by bitcoin has its theoretical roots in the Austrian school of economics, especially with Friedrich von Hayek in his book Denationalisation of Money: The Argument Refined, in which Hayek advocates a complete free market in the production, distribution and management of money to end the monopoly of central banks.

Bitcoin is a digital asset designed to work in peer-to-peer transactions as a currency. Bitcoins have three qualities useful in a currency, according to The Economist in January 2015: they are "hard to earn, limited in supply and easy to verify." Per some researchers, as of 2015, bitcoin functions more as a payment system than as a currency.

Economists define money as a store of value, a medium of exchange, and a unit of account. According to The Economist in 2014, bitcoin functions best as a medium of exchange. However, this is debated, and a 2018 assessment by The Economist stated that cryptocurrencies met none of these three criteria. Yale economist Robert J. Shiller writes that bitcoin has potential as a unit of account for measuring the relative value of goods, as with Chile's Unidad de Fomento, but that "Bitcoin in its present form [...] doesn’t really solve any sensible economic problem".

## References and Further Reading for Chapter 5

[https://web.archive.org/web/20180615010015/http://www.businessinsider.com/bitcoin-history-cryptocurrency-satoshi-nakamoto-2017-12]()

[https://bitcoin.org/bitcoin.pdf](https://bitcoin.org/bitcoin.pdf)

[https://www.wired.com/story/after-10-years-bitcoin-changed-everything-nothing/](https://www.wired.com/story/after-10-years-bitcoin-changed-everything-nothing/)

[https://web.archive.org/web/20131031043919/http://www.wired.com:80/magazine/2011/11/mf_bitcoin](https://web.archive.org/web/20131031043919/http://www.wired.com:80/magazine/2011/11/mf_bitcoin)

[http://article.gmane.org/gmane.comp.encryption.general/12588/](http://article.gmane.org/gmane.comp.encryption.general/12588/)

[http://www.mail-archive.com/search?l=cryptography@metzdowd.com&q=from:%22Satoshi+Nakamoto%22](http://www.mail-archive.com/search?l=cryptography@metzdowd.com&q=from:%22Satoshi+Nakamoto%22)

[https://web.archive.org/web/20131015154613/http://blockexplorer.com:80/block/000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f](https://web.archive.org/web/20131015154613/http://blockexplorer.com:80/block/000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f)

[https://www.webcitation.org/6J555dPUv](https://www.webcitation.org/6J555dPUv)

[https://www.thetimes.co.uk/article/chancellor-alistair-darling-on-brink-of-second-bailout-for-banks-n9l382mn62h](https://www.thetimes.co.uk/article/chancellor-alistair-darling-on-brink-of-second-bailout-for-banks-n9l382mn62h)

[https://web.archive.org/web/20180121071329/https://books.google.com.au/books?id=\_-wuBAAAQBAJ](https://web.archive.org/web/20180121071329/https://books.google.com.au/books?id=_-wuBAAAQBAJ)

[https://web.archive.org/web/20140326174921/http://www.mail-archive.com/cryptography@metzdowd.com/msg10142.html](https://web.archive.org/web/20140326174921/http://www.mail-archive.com/cryptography@metzdowd.com/msg10142.html)

[https://web.archive.org/web/20130316013625/http://sourceforge.net:80/news/?group_id=244765](https://web.archive.org/web/20130316013625/http://sourceforge.net:80/news/?group_id=244765)

[https://www.washingtonpost.com/blogs/the-switch/wp/2014/01/03/hal-finney-received-the-first-bitcoin-transaction-heres-how-he-describes-it/](https://www.washingtonpost.com/blogs/the-switch/wp/2014/01/03/hal-finney-received-the-first-bitcoin-transaction-heres-how-he-describes-it/)

[https://www.nytimes.com/2014/08/31/business/hal-finney-cryptographer-and-bitcoin-pioneer-dies-at-58.html?\_r=1](https://www.nytimes.com/2014/08/31/business/hal-finney-cryptographer-and-bitcoin-pioneer-dies-at-58.html?_r=1)

[https://www.wired.com/2013/12/fbi_wallet/](https://www.wired.com/2013/12/fbi_wallet/)

[http://www.huffingtonpost.com/2013/04/16/gavin-andresen-bitcoin_n_3093316.html](http://www.huffingtonpost.com/2013/04/16/gavin-andresen-bitcoin_n_3093316.html)

[https://web.archive.org/web/20140409025103/http://www.monetarism.co.uk/the-beginners-guide-to-bitcoin-everything-you-need-to-know/](https://web.archive.org/web/20140409025103/http://www.monetarism.co.uk/the-beginners-guide-to-bitcoin-everything-you-need-to-know/)

[https://web.archive.org/web/20140409034542/https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2010-5139](https://web.archive.org/web/20140409034542/https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2010-5139)

[https://web.archive.org/web/20131015232402/http://sourceforge.net/p/bitcoin/mailman/bitcoin-list/?viewmonth=201008](https://web.archive.org/web/20131015232402/http://sourceforge.net/p/bitcoin/mailman/bitcoin-list/?viewmonth=201008)

[https://web.archive.org/web/20150618072429/http://edition.cnn.com/2014/06/18/business/bitcoin-your-way-to-a-double-espresso/](https://web.archive.org/web/20150618072429/http://edition.cnn.com/2014/06/18/business/bitcoin-your-way-to-a-double-espresso/)

[https://blockchair.com/](https://blockchair.com/)

[https://web.archive.org/web/20141103020741/http://blockchain.info/charts](https://web.archive.org/web/20141103020741/http://blockchain.info/charts)

[https://web.archive.org/web/20160703000844/http://www.economist.com/news/briefing/21677228-technology-behind-bitcoin-lets-people-who-do-not-know-or-trust-each-other-build-dependable](https://web.archive.org/web/20160703000844/http://www.economist.com/news/briefing/21677228-technology-behind-bitcoin-lets-people-who-do-not-know-or-trust-each-other-build-dependable)

[https://web.archive.org/web/20160509053819/http://www.econinfosec.org/archive/weis2013/papers/KrollDaveyFeltenWEIS2013.pdf](https://web.archive.org/web/20160509053819/http://www.econinfosec.org/archive/weis2013/papers/KrollDaveyFeltenWEIS2013.pdf)

[https://web.archive.org/web/20140115062630/http://washington.cbslocal.com/2013/11/29/man-throws-away-7500-bitcoins-now-worth-7-5-million/](https://web.archive.org/web/20140115062630/http://washington.cbslocal.com/2013/11/29/man-throws-away-7500-bitcoins-now-worth-7-5-million/)

[https://web.archive.org/web/20180709010939/https://www.wsj.com/articles/a-fifth-of-all-bitcoin-is-missing-these-crypto-hunters-can-help-1530798731](https://web.archive.org/web/20180709010939/https://www.wsj.com/articles/a-fifth-of-all-bitcoin-is-missing-these-crypto-hunters-can-help-1530798731)

[https://www.digitaltrends.com/computing/who-owns-all-the-bitcoin/](https://www.digitaltrends.com/computing/who-owns-all-the-bitcoin/)

[https://web.archive.org/web/20180202070911/https://www.theverge.com/2018/1/30/16949550/bitcoin-graphics-cards-pc-prices-surge](https://web.archive.org/web/20180202070911/https://www.theverge.com/2018/1/30/16949550/bitcoin-graphics-cards-pc-prices-surge)

[https://web.archive.org/web/20180130091353/http://www.worldoil.com/news/2018/1/26/cryptocurrency-mining-operation-launched-by-iron-bridge-resources](https://web.archive.org/web/20180130091353/http://www.worldoil.com/news/2018/1/26/cryptocurrency-mining-operation-launched-by-iron-bridge-resources)

[https://web.archive.org/web/20140409044505/http://www.stlouisfed.org/dialogue-with-the-fed/assets/Bitcoin-3-31-14.pdf](https://web.archive.org/web/20140409044505/http://www.stlouisfed.org/dialogue-with-the-fed/assets/Bitcoin-3-31-14.pdf)

_Sherman, Alan; Javani, Farid; Golaszewski, Enis (25 March 2019). "On the Origins and Variations of Blockchain Technologies". IEEE Security and Policy. 17 (1): 72–77. arXiv:1810.06130. doi:10.1109/MSEC.2019.2893730._

[https://web.archive.org/web/20150408212548/https://blockchain.info/charts/difficulty?timespan=all&showDataPoints=false&daysAverageString=1&show_header=true&scale=0&address=](https://web.archive.org/web/20150408212548/https://blockchain.info/charts/difficulty?timespan=all&showDataPoints=false&daysAverageString=1&show_header=true&scale=0&address=)

[https://web.archive.org/web/20131121225123/http://www.businessweek.com/articles/2013-11-14/2014-outlook-bitcoin-mining-chips-a-high-tech-arms-race](https://web.archive.org/web/20131121225123/http://www.businessweek.com/articles/2013-11-14/2014-outlook-bitcoin-mining-chips-a-high-tech-arms-race)

[https://web.archive.org/web/20160918115932/https://blockchain.info/block/000000000000000002cce816c0ab2c5c269cb081896b7dcb34b8422d6b74ffa1](https://web.archive.org/web/20160918115932/https://blockchain.info/block/000000000000000002cce816c0ab2c5c269cb081896b7dcb34b8422d6b74ffa1)

[https://web.archive.org/web/20131217221429/http://qz.com/154877/by-reading-this-page-you-are-mining-bitcoins/](https://web.archive.org/web/20131217221429/http://qz.com/154877/by-reading-this-page-you-are-mining-bitcoins/)

[https://web.archive.org/web/20160524141916/http://www.forbes.com/sites/laurashin/2016/05/24/bitcoin-production-will-drop-by-half-in-july-how-will-that-affect-the-price/](https://web.archive.org/web/20160524141916/http://www.forbes.com/sites/laurashin/2016/05/24/bitcoin-production-will-drop-by-half-in-july-how-will-that-affect-the-price/)

[https://web.archive.org/web/20161009183700/https://www.fincen.gov/sites/default/files/2016-08/20131118.pdf](https://web.archive.org/web/20161009183700/https://www.fincen.gov/sites/default/files/2016-08/20131118.pdf)

[https://web.archive.org/web/20141101014157/http://www.newyorker.com/magazine/2011/10/10/the-crypto-currency](https://web.archive.org/web/20141101014157/http://www.newyorker.com/magazine/2011/10/10/the-crypto-currency)

[https://web.archive.org/web/20180427142806/http://www.businessinsider.com/blockchain-distributed-ledgers-2017-10](https://web.archive.org/web/20180427142806/http://www.businessinsider.com/blockchain-distributed-ledgers-2017-10)

[https://web.archive.org/web/20130921060724/http://mercatus.org/sites/default/files/Brito_BitcoinPrimer.pdf](https://web.archive.org/web/20130921060724/http://mercatus.org/sites/default/files/Brito_BitcoinPrimer.pdf)

[https://doi.org/10.1109%2Fcomst.2016.2535718](https://doi.org/10.1109%2Fcomst.2016.2535718)

_Beikverdi, A.; Song, J. (June 2015). Trend of centralization in Bitcoin's distributed network. 2015 IEEE/ACIS 16th International Conference on Software Engineering, Artificial Intelligence, Networking and Parallel/Distributed Computing (SNPD). pp. 1–6. doi:10.1109/SNPD.2015.7176229. ISBN 978-1-4799-8676-7._

[https://web.archive.org/web/20161010042659/https://www.infoq.com/articles/is-bitcoin-a-decentralized-currency/](https://web.archive.org/web/20161010042659/https://www.infoq.com/articles/is-bitcoin-a-decentralized-currency/)

[https://web.archive.org/web/20180701222302/https://www.nytimes.com/2013/12/15/sunday-review/the-bitcoin-ideology.html](https://web.archive.org/web/20180701222302/https://www.nytimes.com/2013/12/15/sunday-review/the-bitcoin-ideology.html)

[https://archive.org/details/denationalisatio0000haye](https://archive.org/details/denationalisatio0000haye)

[https://web.archive.org/web/20121106053452/http://www.ecb.europa.eu/pub/pdf/other/virtualcurrencyschemes201210en.pdf](https://web.archive.org/web/20121106053452/http://www.ecb.europa.eu/pub/pdf/other/virtualcurrencyschemes201210en.pdf)

[https://web.archive.org/web/20131020192331/http://www.economist.com/node/21563752](https://web.archive.org/web/20131020192331/http://www.economist.com/node/21563752)

[https://web.archive.org/web/20150112165531/http://www.economist.com/news/business/21638124-minting-digital-currency-has-become-big-ruthlessly-competitive-business-magic](https://web.archive.org/web/20150112165531/http://www.economist.com/news/business/21638124-minting-digital-currency-has-become-big-ruthlessly-competitive-business-magic)

[https://web.archive.org/web/20140325085119/http://www.economist.com/news/finance-and-economics/21599053-chronic-deflation-may-keep-bitcoin-displacing-its-fiat-rivals-money](https://web.archive.org/web/20140325085119/http://www.economist.com/news/finance-and-economics/21599053-chronic-deflation-may-keep-bitcoin-displacing-its-fiat-rivals-money)

[https://web.archive.org/web/20141024120222/http://www.nytimes.com/2014/03/02/business/in-search-of-a-stable-electronic-currency.html](https://web.archive.org/web/20141024120222/http://www.nytimes.com/2014/03/02/business/in-search-of-a-stable-electronic-currency.html)

[https://www.businessinsider.com/why-bitcoin-has-the-potential-to-blow-the-legacy-payments-system-out-of-the-water-2-2014-2](https://www.businessinsider.com/why-bitcoin-has-the-potential-to-blow-the-legacy-payments-system-out-of-the-water-2-2014-2)

# 6. Deep Look at Ethereum

Ethereum was initially described in a white paper by Vitalik Buterin, a programmer involved with Bitcoin Magazine, in late 2013 with a goal of building decentralized applications. Buterin had argued that Bitcoin needed a scripting language for application development. Failing to gain agreement, he proposed development of a new platform with a more general scripting language.

Ethereum was announced at the North American Bitcoin Conference in Miami, in January, 2014. During the same time as the conference, a group of people rented a house in Miami: Gavin Wood, Charles Hoskinson, and Anthony Di Iorio, a Torontonian who financed the project. Di Iorio invited friend Joseph Lubin, who invited reporter Morgen Peck, to bear witness. Six months later the founders met again in a house in Zug, Switzerland, where Buterin told the founders that the project would proceed as a non-profit. Hoskinson left the project at that time.

Ethereum has an unusually long list of founders. Anthony Di Iorio wrote "Ethereum was founded by Vitalik Buterin, Myself, Charles Hoskinson, Mihai Alisie, & Amir Chetrit (the initial 5) in December 2013. Joseph Lubin, Gavin Wood, & Jeffrey Wilke were added in early 2014 as founders." Formal development of the Ethereum software project began in early 2014 through a Swiss company, Ethereum Switzerland GmbH (EthSuisse). The basic idea of putting executable smart contracts in the blockchain needed to be specified before the software could be implemented; this work was done by Gavin Wood, then chief technology officer, in the Ethereum Yellow Paper that specified the Ethereum Virtual Machine. Subsequently, a Swiss non-profit foundation, the Ethereum Foundation (Stiftung Ethereum), was created as well. Development was funded by an online public crowdsale during July–August 2014, with the participants buying the Ethereum value token (ether) with another digital currency, Bitcoin.

While there was early praise for the technical innovations of Ethereum, questions were also raised about its security and scalability.

In March 2017, various blockchain start-ups, research groups, and Fortune 500 companies announced the creation of the Enterprise Ethereum Alliance (EEA) with 30 founding members. By May, the nonprofit organization had 116 enterprise members—including ConsenSys, CME Group, Cornell University's research group, Toyota Research Institute, Samsung SDS, Microsoft, Intel, J. P. Morgan, Cooley LLP, Merck KGaA, DTCC, Deloitte, Accenture, Banco Santander, BNY Mellon, ING, and National Bank of Canada. By July 2017, there were over 150 members in the alliance, including recent additions MasterCard, Cisco Systems, Sberbank and Scotiabank.

Several codenamed prototypes of the Ethereum platform were developed by the Foundation, as part of their Proof-of-Concept series, prior to the official launch of the Frontier network. "Olympic" was the last of these prototypes, and public beta pre-release. The Olympic network provided users with a bug bounty of 25,000 ether for stress testing the limits of the Ethereum blockchain. "Frontier" marked the tentative experimental release of the Ethereum platform in July 2015.

Since the initial launch, Ethereum has undergone several planned protocol upgrades, which are important changes affecting the underlying functionality and/or incentive structures of the platform. Protocol upgrades are accomplished by means of a soft fork of the open source code base:

"Homestead" was the first to be considered stable. It included improvements to transaction processing, gas pricing, and security; and the soft fork[citation needed] occurred on 31 July 2015.

The "Metropolis Part 1: Byzantium" soft fork took effect on 16 October 2017, and included changes to reduce the complexity of the EVM and provide more flexibility for smart contract developers. Byzantium also added supports for zk-SNARKs (from Zcash), with the first zk-SNARK transaction occurring on testnet on September 19, 2017.

The "Metropolis Part 2: Constantinople" hard fork, and the simultaneous "Petersburg" network upgrade, occurred at block number 7,280,000 on February 28, 2019.

In 2016 a decentralized autonomous organization called The DAO, a set of smart contracts developed on the platform, raised a record US$150 million in a crowdsale to fund the project. The DAO was exploited in June when US$50 million in ether were taken by an unknown hacker. The event sparked a debate in the crypto-community about whether Ethereum should perform a contentious "hard fork" to reappropriate the affected funds. As a result of the dispute, the network split in two. Ethereum (the subject of this article) continued on the forked blockchain, while Ethereum Classic continued on the original blockchain. The hard fork created a rivalry between the two networks.

After the hard fork related to The DAO, Ethereum subsequently forked twice in the fourth quarter of 2016 to deal with other attacks. By the end of November 2016, Ethereum had increased its DDoS protection, de-bloated the blockchain, and thwarted further spam attacks by hackers.

As with other cryptocurrencies, the validity of each ether is provided by a blockchain, which is a continuously growing list of records, called blocks, which are linked and secured using cryptography. By design, the blockchain is inherently resistant to modification of the data. It is an open, distributed ledger that records transactions between two parties efficiently and in a verifiable and permanent way. Unlike Bitcoin, Ethereum operates using accounts and balances in a manner called state transitions. This does not rely upon unspent transaction outputs (UTXOs). State denotes the current balances of all accounts and extra data. State is not stored on the blockchain, it is stored in a separate Merkle Patricia tree. A cryptocurrency wallet stores the public and private "keys" or "addresses" which can be used to receive or spend ether. These can be generated through BIP 39 style mnemonics for a BIP 32 "HD Wallet". In Ethereum, this is unnecessary as it does not operate in a UTXO scheme. With the private key, it is possible to write in the blockchain, effectively making an ether transaction.

To send ether to an account, you need the Keccak-256 hash of the public key of that account. Ether accounts are pseudonymous in that they are not linked to individual persons, but rather to one or more specific addresses.

Ether is a fundamental token for operation of Ethereum, which thereby provides a public distributed ledger for transactions. It is used to pay for gas, a unit of computation used in transactions and other state transitions. Mistakenly, this currency is also referred to as Ethereum.

It is listed under the ticker symbol ETH and traded on cryptocurrency exchanges, and the Greek uppercase Xi character (Ξ) is generally used for its currency symbol. It is also used to pay for transaction fees and computational services on the Ethereum network.

Ethereum addresses are composed of the prefix "0x", a common identifier for hexadecimal, concatenated with the rightmost 20 bytes of the Keccak-256 hash (big endian) of the ECDSA public key (the curve used is the so called secp256k1, the same as Bitcoin). In hexadecimal, 2 digits represents a byte, meaning addresses contain 40 hexadecimal digits. An example of an Ethereum address is 0xb794F5eA0ba39494cE839613fffBA74279579268. Contract addresses are in the same format, however they are determined by sender and creation transaction nonce. User accounts are indistinguishable from contract accounts given only an address for each and no blockchain data. Any valid Keccak-256 hash put into the described format is valid, even if it does not correspond to an account with a private key or a contract. This is unlike Bitcoin, which uses base58check to ensure that addresses are properly typed.

Ethereum is different from Bitcoin (the cryptocurrency with the largest market capitalization as of June 2018) in several aspects:

- Its block time is 14 to 15 seconds, compared with 10 minutes for bitcoin.
- Mining of ether generates new coins at a usually consistent rate, occasionally changing during hard forks, while for bitcoin the rate halves every 4 years.
- For proof-of-work, it uses the Ethash algorithm which reduces the advantage of specialized ASICs in mining.
- Transaction fees differ by computational complexity, bandwidth use and storage needs (in a system known as gas), while bitcoin transactions compete by means of transaction size, in bytes.
- Ethereum uses an account system where values in Wei are debited from accounts and credited to another, as opposed to Bitcoin's UTXO system, which is more analogous to spending cash and receiving change in return.

The Ethereum Virtual Machine (EVM) is the runtime environment for smart contracts in Ethereum. It is a 256-bit register stack, designed to run the same code exactly as intended. It is the fundamental consensus mechanism for Ethereum. The formal definition of the EVM is specified in the Ethereum Yellow Paper. On February 1, 2018, there were 27,500 nodes in the main Ethereum network. Ethereum Virtual Machines have been implemented in C++, C#, Go, Haskell, Java, JavaScript, Python, Ruby, Rust, Elixir, Erlang, and soon, WebAssembly (currently under development).

Ethereum's smart contracts are based on different computer languages, which developers use to program their own functionalities. Smart contracts are high-level programming abstractions that are compiled down to EVM bytecode and deployed to the Ethereum blockchain for execution. They can be written in Solidity (a language library with similarities to C and JavaScript), Serpent (similar to Python, but deprecated), LLL (a low-level Lisp-like language), and Mutan (Go-based, but deprecated). There is also a research-oriented language under development called Vyper (a strongly-typed Python-derived decidable language).

Smart contracts can be public, which opens up the possibility to prove functionality, e.g. self-contained provably fair casinos.

One issue related to using smart contracts on a public blockchain is that bugs, including security holes, are visible to all but cannot be fixed quickly. One example of this is the 17 June 2016 attack on The DAO, which could not be quickly stopped or reversed.

There is ongoing research on how to use formal verification to express and prove non-trivial properties. A Microsoft Research report noted that writing solid smart contracts can be extremely difficult in practice, using The DAO hack to illustrate this problem. The report discussed tools that Microsoft had developed for verifying contracts, and noted that a large-scale analysis of published contracts is likely to uncover widespread vulnerabilities. The report also stated that it is possible to verify the equivalence of a Solidity program and the EVM code.

Ethereum apps are written in one of seven different Turing-complete languages. Developers use the language to create and publish applications which they know will run inside Ethereum.

Many uses have been proposed for Ethereum platform, including ones that are impossible or unfeasible. Use case proposals have included finance, the internet-of-things, farm-to-table produce, electricity sourcing and pricing, and sports betting.

Ethereum-based customized software and networks, independent from the public Ethereum chain, are being tested by enterprise software companies. Interested parties include Microsoft, IBM, JPMorgan Chase, Deloitte, R3, Innovate UK (cross-border payments prototype). Barclays, UBS and Credit Suisse are experimenting with Ethereum.

In Ethereum all smart contracts are stored publicly on every node of the blockchain, which has costs. Being a blockchain means it is secure by design and is an example of a distributed computing system with high Byzantine fault tolerance. The downside is that performance issues arise in that every node is calculating all the smart contracts in real time, resulting in lower speeds. As of January 2016, the Ethereum protocol could process about 25 transactions per second. In comparison, the Visa payment platform processes 45,000 payments per second leading some to question the scalability of Ethereum. On 19 December 2016, Ethereum exceeded one million transactions in a single day for the first time.

Ethereum engineers have been working on sharding the calculations, and the next step (called Ethereum 2) was presented at Ethereum's Devcon 3 in November 2017.

Ethereum's blockchain uses Merkle trees, for security reasons, to improve scalability, and to optimize transaction hashing. As with any Merkle tree implementation, it allows for storage savings, set membership proofs (called "Merkle proofs"), and light client synchronization. The Ethereum network has at times faced congestion problems, for example, congestion occurred during late 2017 in relation to Cryptokitties.

In October 2015, a development governance was proposed as Ethereum Improvement Proposal, aka EIP, standardized on EIP-1. The core development group and community were to gain consensus by a process regulated EIP. A few notable decisions were made in the process of EIP, such as EIP-160 (EXP cost increase caused by Spurious Dragon Hardfork) and EIP-20 (ERC-20 Token Standard). In January 2018, the EIP process was finalized and published as EIP-1 status turned "active". Alongside ERC-20, notable EIPs to have become finalised token standards include ERC-721 (enabling the creation of non-fungible tokens, as used in Cryptokitties) and as of June 2019, ERC-1155 (enabling the creation of both fungible and non-fungible tokens within a single smart contract with reduced gas costs).

## References and Further Reading for Chapter 6

https://web.archive.org/web/20140111180823/http://ethereum.org/ethereum.html

https://web.archive.org/web/20160318170702/http://www.wired.com/2014/01/ethereum/

https://web.archive.org/web/20160223191510/http://america.aljazeera.com/articles/2014/4/7/code-your-own-utopiameetethereumbitcoinasmostambitioussuccessor.html

https://web.archive.org/web/20160425215408/http://www.theepochtimes.com/n3/668104-the-entrepreneur-joe-lubin-coo-of-ethereum/

https://web.archive.org/web/20160820112644/http://www.bloomberg.com/Research/stocks/private/snapshot.asp?privcapId=309484729

https://books.google.com/books?id=roVkDgAAQBAJ&pg=PA30&lpg=PA30&dq#v=onepage&q&f=false

https://web.archive.org/web/20170620134100/https://www.nytimes.com/2017/02/27/business/dealbook/ethereum-alliance-business-banking-security.html

https://web.archive.org/web/20170617073826/http://spectrum.ieee.org/tech-talk/computing/networks/enterprise-ethereum-alliance-launches

https://web.archive.org/web/20170607000537/https://entethalliance.org/enterprise-ethereum-alliance-release-05-19-2017.pdf

https://web.archive.org/web/20170522060059/https://www.forbes.com/sites/laurashin/2017/05/22/ethereum-enterprise-alliance-adds-86-new-members-including-dtcc-state-street-and-infosys-and/#7de2037e8ff2

https://www.inc.com/brian-d-evans/the-enterprise-ethereum-alliance-just-got-a-whole-.html

https://www.bankingtech.com/2017/10/sberbank-joins-enterprise-ethereum-alliance-to-broaden-cooperation/

https://web.archive.org/web/20170906093642/https://blogs.wsj.com/moneybeat/2015/07/31/bitbeat-ethereum-opens-its-frontier-for-business/

https://web.archive.org/web/20170606200002/https://blog.ethereum.org/2015/03/03/ethereum-launch-process/

https://web.archive.org/web/20170625083255/https://www.wsj.com/articles/chiefless-company-rakes-in-more-than-100-million-1463399393

https://web.archive.org/web/20170620012726/https://www.nytimes.com/2016/06/18/business/dealbook/hacker-may-have-removed-more-than-50-million-from-experimental-cybercurrency-project.html

https://web.archive.org/web/20170611195628/http://uk.businessinsider.com/dao-hacked-ethereum-crashing-in-value-tens-of-millions-allegedly-stolen-2016-6

https://web.archive.org/web/20170303002838/http://spectrum.ieee.org/tech-talk/computing/networks/hacked-blockchain-fund-the-dao-chooses-a-hard-fork-to-redistribute-funds

Narayanan, Arvind; Bonneau, Joseph; Felten, Edward; Miller, Andrew; Goldfeder, Steven (2016). Bitcoin and Cryptocurrency Technologies: a Comprehensive Introduction. Princeton: Princeton University Press. ISBN 978-0-691-17169-2.

https://hbr.org/2017/01/the-truth-about-blockchain

https://web.archive.org/web/20160724214904/http://www.nytimes.com/2016/03/28/business/dealbook/ethereum-a-virtual-currency-enables-transactions-that-rival-bitcoins.html

https://web.archive.org/web/20180203110042/http://yellowpaper.io/

https://thenextweb.com/hardfork/2018/06/11/ethereums-total-supply/

https://www.ethernodes.org/network/1

https://web.archive.org/web/20161224031048/http://www.ledgerjournal.org/ojs/index.php/ledger/article/view/29

https://web.archive.org/web/20160530202345/http://spectrum.ieee.org/tech-talk/computing/networks/ethereums-150-million-dollar-dao-opens-for-business-just-as-researchers-call-for-a-moratorium

https://web.archive.org/web/20160827092146/http://research.microsoft.com/en-us/um/people/nswamy/papers/solidether.pdf

https://www.economist.com/sites/default/files/creighton_university_kraken_case_study.pdf

https://www.investopedia.com/articles/investing/031416/bitcoin-vs-ethereum-driven-different-purposes.asp

http://www.perfecttrendsystem.com/litecoin-vs-ethereum

https://web.archive.org/web/20160914101357/http://www.bloomberg.com/news/articles/2016-08-25/this-is-your-company-on-blockchain

https://web.archive.org/web/20170929173431/http://fortune.com/2017/02/28/ethereum-jpmorgan-microsoft-alliance/

https://web.archive.org/web/20160506014539/http://www.ibtimes.co.uk/deloitte-build-ethereum-based-digital-bank-new-york-citys-consensys-1557864

https://web.archive.org/web/20160223021945/http://www.ibtimes.co.uk/r3-connects-11-banks-distributed-ledger-using-ethereum-microsoft-azure-1539044

https://web.archive.org/web/20160504022052/http://gtr.rcuk.ac.uk/projects?ref=720735

https://web.archive.org/web/20171109080854/http://www.bankingtech.com/1037032/jp-morgans-quorum-blockchain-powers-new-correspondent-banking-network/

https://web.archive.org/web/20170202033844/http://fortune.com/2016/10/04/jp-morgan-chase-blockchain-ethereum-quorum/

https://web.archive.org/web/20161010182558/https://emerald-platform.gitlab.io/static/emeraldTechnicalPaper.pdf

https://web.archive.org/web/20170413061330/http://www.ibtimes.co.uk/rbs-builds-ethereum-based-distributed-clearing-house-1589897

https://web.archive.org/web/20160508064831/http://www.ibtimes.co.uk/how-are-banks-actually-going-use-blockchains-smart-contracts-1539789

https://web.archive.org/web/20171222052607/https://discover.coinsquare.io/tech/ethereum-one-million-transaction-day/

https://web.archive.org/web/20171109090333/https://futurism.com/ethereums-co-founder-just-unveiled-his-plan-for-the-future-of-cryptocurrency/

https://blog.ethereum.org/2015/11/15/merkling-in-ethereum

https://qz.com/1145833/cryptokitties-is-causing-ethereum-network-congestion/

https://github.com/ethereum/EIPs/commit/db14da1956b857680477c48a85a3d566f69aa2e4#diff-2f1874f8ac5135980a12a961cfafc079

https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md

https://github.com/ethereum/EIPs/issues/721

https://coinrivet.com/ethereum-adopts-erc-1155-as-an-official-standard/

# 7. Deep Look at NEM

NEM was started by a Bitcoin Talk forum user called UtopianFuture who was inspired by Nxt. The initial plan for NEM was to create a fork of NXT, but this was eventually dismissed in favor of a completely new codebase. Starting on January 19, 2014, an open call for participation began on the Bitcointalk forum. The goal of the call was to create a community-oriented cryptocurrency from the ground up.

NEM Foundation; created in 2016, ~2 years after the Public Chain launch. It was funded from one of the reserved pools. It is a Company Limited by Guarantee in Singapore. Its early focus was on brand awareness, marketing, training and partnerships. In late 2018 the first council’s term came to an end and a new council was elected by members.

On 26 January 2018, Japanese cryptocurrency exchange Coincheck was the victim of a massive hack resulting in a loss of 523 million XEM coins, the native token of NEM, worth approximately \$400 million. The NEM team created an automated tagging system. This automated system followed the money and tagged any account that received tainted money.

The result of these actions was that NEM stopped tracking the stolen coins approximately mid-March 2018, after concluding that enough data was provided to the law enforcement authorities.

In July 2018, the Ukraine Central Election Commission began investigating the use of blockchain technology in elections using the NEM platform. The tests were performed in a test environment with 28 nodes, using test coins provided by the NEM Foundation. The commission estimated that a cost of a node would be approximately \$1,227, which was described as a “small” price to pay for the technology. The test proved NEM’s potential utility in voting.

In November 2018, Malaysia’s Ministry of Education formed a consortium of universities to use NEM’s blockchain technology to authenticate academic certificates. The consortium was created to combat the rise of fraudulent fake degree certificates and to optimize certificate authentication. The ministry stated that NEM was chosen because of its “unique features in managing traceability and authentication requirements”.

NEM's design architecture consists of two components. One is the node or NEM Infrastructure Server (NIS). The second is the client used for interacting with the nodes, its cryptocurrency wallet, the NanoWallet which is built with HTML and Javascript.

Another client was the NEM Community Client (NCC). The NIS is connected to the P2P network and acts as a gateway for the NCC. The NCC is client software that includes a wallet. The NCC has since been deprecated in favor of the NanoWallet. Both NCC and the NanoWallet can be run isolated from the internet, providing security through an airgap.

Namespaces allows users to own domain names on the NEM blockchain just like a person or an organization owns an Internet domain name. Just like on the internet, a domain can have a sub-domain, namespaces can have sub-namespaces. And it is possible to create multiple sub-namespaces with the same name (example: “foo.bar” and “foo2.bar”, “bar” is the sub-namespace/sub-domain). A namespace and a domain name is the same in this document and shall be used interchangeably. Namespaces can have up to 3 levels, a namespace and its two levels of sub-namespace domains.

NEM network implements a modified version of Eigentrust++ to identify and minimize the impact of malicious nodes. Eigentrust++ is a security clustering algorithm which monitors past behavior of nodes in the network and enables nodes to give reputation to their neighbors in clusters. In proof-of-work, the amount of work a node does is used as a measure for its ability to protect the network. But, with Eigentrust++, it is the quality of work that is important. This adds to the NEM network's ability to be run and maintained efficiently.

NEM uses a "proof of importance" (POI) to score how people can harvest XEM; a person has to have 10,000 XEM in their balance to be scored, and the number of transactions they have with others, to time stamp transactions. This was designed to encourage users of NEM to not simply hold XEM but instead actively carry out transactions.

NEM's multi-signature contracts are universal, meaning they are built into the blockchain, not on top of the blockchain as in third-party reliant software. This unlocks various advantages and interesting possibilities for the user. The contract enables several people to administrate the activity of an account, control assets such as XEM from one account, other mosaics, or create additional contracts such as creating a new token.

## References and Further Reading for Chapter 7

http://motherboard.vice.com/read/this-cryptocurrency-doesnt-want-to-beat-bitcoin-it-wants-to-beat-the-economy

https://nemventures.io/nem-ecosystem-explained/

https://www.forbes.com/sites/outofasia/2018/01/29/tracing-back-stolen-cryptocurrency-xem-from-japans-coincheck/#2b94ba0c277e

https://www.reuters.com/article/us-japan-cryptocurrency/japan-raps-coincheck-orders-broader-checks-after-530-million-cryptocurrency-theft-idUSKBN1FI06S

https://www.japantimes.co.jp/news/2018/03/21/business/tech/singapore-based-nem-foundation-ends-tracking-stolen-cryptocurrency/#.WvVpmsixVQI

https://www.tradingpeek.com/en/crypto/details/Crypto-news/1039/%E2%80%8BOfficial-Elections-Ukraine-launches-a-voting-experience-using-NEM-Blockchain

https://www.thinkdigitalpartners.com/news/2018/08/09/ukraine-trial-blockchain-based-voting/

https://www.thestar.com.my/news/nation/2018/11/10/fake-degrees-wont-make-the-cut-with-escroll-system/

http://ekmair.ukma.edu.ua/bitstream/handle/123456789/14628/Technical_comparison_aspects_of_leading_blockchain_based.pdf?sequence=1&isAllowed=y

http://ekmair.ukma.edu.ua/bitstream/handle/123456789/14628/Technical_comparison_aspects_of_leading_blockchain_based.pdf?sequence=1&isAllowed=y

http://motherboard.vice.com/read/this-cryptocurrency-doesnt-want-to-beat-bitcoin-it-wants-to-beat-the-economy

http://ekmair.ukma.edu.ua/bitstream/handle/123456789/14628/Technical_comparison_aspects_of_leading_blockchain_based.pdf?sequence=1&isAllowed=y

https://docs.nem.io/en/nanowallet/multisignature-multiuser

https://nem.io/NEM_techRef.pdf

# 8. Smart Contracts

Smart contracts were first proposed in the early 1990s by computer scientist, lawyer and cryptographer Nick Szabo, who coined the term. With the present implementations, based on blockchains, "smart contract" is mostly used more specifically in the sense of general purpose computation that takes place on a blockchain or distributed ledger. In this interpretation, used for example by the Ethereum Foundation or IBM, a smart contract is not necessarily related to the classical concept of a contract, but can be any kind of computer program.

A smart contract also can be regarded as a secured stored procedure as its execution and codified effects like the transfer of some value between parties are strictly enforced and can not be manipulated, after a transaction with specific contract details is stored into a blockchain or distributed ledger. That's because the actual execution of contracts is controlled and audited by the platform, not by any arbitrary server-side programs connecting to the platform.

In 2018, a US Senate report said: "While smart contracts might sound new, the concept is rooted in basic contract law. Usually, the judicial system adjudicates contractual disputes and enforces terms, but it is also common to have another arbitration method, especially for international transactions. With smart contracts, a program enforces the contract built into the code."

By implementing the Decree on Development of Digital Economy, Belarus has become the first-ever country to legalize smart contracts. Belarusian lawyer Denis Aleinikov is considered to be the author of a smart contract legal concept introduced by the decree.

Byzantine fault-tolerant algorithms allowed digital security through decentralization to form smart contracts. Additionally, the programming languages with various degrees of Turing-completeness as a built-in feature of some blockchains make the creation of custom sophisticated logic possible.

Notable examples of implementation of smart contracts include the Bitcoin and Ethereum. Bitcoin provides a Turing-incomplete Script language that allows the creation of custom smart contracts on top of Bitcoin like multisignature accounts, payment channels, escrows, time locks, atomic cross-chain trading, oracles, or multi-party lottery with no operator. Ethereum implements a nearly Turing-complete language on its blockchain, a prominent smart contract framework.

Szabo proposes that smart contract infrastructure can be implemented by replicated asset registries and contract execution using cryptographic hash chains and Byzantine fault-tolerant replication. Askemos implemented this approach in 2002 using Scheme (later adding SQLite) as contract script language.

One proposal for using bitcoin for replicated asset registration and contract execution is called "colored coins". Replicated titles for potentially arbitrary forms of property, along with replicated contract execution, are implemented in different projects.

As of 2015, UBS was experimenting with "smart bonds" that use the bitcoin blockchain in which payment streams could hypothetically be fully automated, creating a self-paying instrument.

A smart contract is "a computerized transaction protocol that executes the terms of a contract". A blockchain-based smart contract is visible to all users of said blockchain. However, this leads to a situation where bugs, including security holes, are visible to all yet may not be quickly fixed.

Such an attack, difficult to fix quickly, was successfully executed on The DAO in June 2016, draining US\$50 million in Ether while developers attempted to come to a solution that would gain consensus. The DAO program had a time delay in place before the hacker could remove the funds; a hard fork of the Ethereum software was done to claw back the funds from the attacker before the time limit expired.

Issues in Ethereum smart contracts, in particular, include ambiguities and easy-but-insecure constructs in its contract language Solidity, compiler bugs, Ethereum Virtual Machine bugs, attacks on the blockchain network, the immutability of bugs and that there is no central source documenting known vulnerabilities, attacks and problematic constructs.

## References and Further Reading for Chapter 8

http://fortune.com/2014/01/21/bitcoin-is-not-just-digital-currency-its-napster-for-finance/

https://github.com/ethereum/wiki/wiki/White-Paper/f18902f4e7fb21dc92b37e8a0963eec4b3f4793a

https://www.zurich.ibm.com/dccl/papers/cachin_dccl.pdf

https://openproceedings.org/2018/conf/edbt/paper-227.pdf

_Huckle, Steve; Bhattacharya, Rituparna; White, Martin; Beloff, Natalia (2016). "Internet of Things, Blockchain and Shared Economy Applications". Procedia Computer Science. Elsevier B.V. 98: 463. doi:10.1016/j.procs.2016.09.074._

https://www.jec.senate.gov/public/_cache/files/aaac3a69-e9fb-45b6-be9f-b1fd96dd738b/chapter-9-building-a-secure-future-one-blockchain-at-a-time.pdf

https://www.reuters.com/article/us-belarus-cryptocurrency-idUSKBN1EG0XO

https://www2.deloitte.com/content/dam/Deloitte/ru/Documents/tax/lt-in-focus/english/2017/27-12-en.pdf

_Governatori, Guido; Idelberger, Florian; Milosevic, Zoran; Riveret, Regis; Sartor, Giovanni; Xu, Xiwei (2018). "On legal contracts, imperative and declarative smart contracts, and blockchain systems". Artificial Intelligence and Law. 26 (4): 33. doi:10.1007/s10506-018-9223-3._

https://eprint.iacr.org/2018/192.pdf

http://eprint.iacr.org/2016/1007.pdf

https://web.archive.org/web/20140115142013/http://szabo.best.vwh.net/securetitle.html

http://citeseerx.ist.psu.edu/viewdoc/download;?doi=10.1.1.11.5050&rep=rep1&type=pdf

http://www.isl.cs.waseda.ac.jp/~sugawara/pdf/kurihara-SSGRR2002.pdf

https://www.newscientist.com/article/dn24620-bitcoin-moves-beyond-mere-money.html

http://europe.newsweek.com/smart-money-blockchains-are-future-internet-329278

http://www.ifrasia.com/bitcoin-technology-will-disrupt-derivatives-says-banker/21202956.article

http://spectrum.ieee.org/tech-talk/computing/networks/ethereums-150-million-dollar-dao-opens-for-business-just-as-researchers-call-for-a-moratorium

https://web.archive.org/web/20170730133911/http://iqdupont.com/assets/documents/DUPONT-2017-Preprint-Algorithmic-Governance.pdf

# 9. A Look at Cryptokitties

CryptoKitties is a blockchain game on Ethereum developed by Axiom Zen that allows players to purchase, collect, breed and sell virtual cats. It is one of the earliest attempts to deploy blockchain technology for recreation and leisure. The game's popularity in December 2017 congested the Ethereum network, causing it to reach an all-time high in number of transactions and slowing it down significantly.

CryptoKitties is not a cryptocurrency. Instead it operates on Ethereum's underlying blockchain network, as a non-fungible token (NFT) unique to each CryptoKitty. Each CryptoKitty is unique and owned by the user, validated through the blockchain, and its value can appreciate or depreciate based on the market.

CryptoKitties cannot be replicated and cannot be transferred without the user's permission even by the game developers. Users can interact with their CryptoKitties, having the ability to buy, sell, and sire (breed) them. However, the CryptoKitty art is not on the blockchain and is instead owned by Axiom Zen. The company released some of the art under a new 'Nifty' license that lets players use the image of their CryptoKitty in a limited way. A test version of CryptoKitties was unveiled at ETH Waterloo on October 19, 2017, an Ethereum hackathon. As of December 2, 2017, Genesis, the first and highest selling cat was sold for 246.9255 ETH (~\$117,712 USD) on that day.

The virtual cats are breedable and carry a unique number and 256 bit distinct genome with DNA and different attributes (cattributes) that can be passed to offspring. Several traits can be passed down from the parents to the offspring. There are a total of 12 'cattributes' for any cat, including pattern, mouth shape, fur, eye shape, base color, accent color, highlight color, eye color, and optional wild, environment, 'purrstige' and 'secret'. Other features like cool down times are not passed down but are instead a function of the 'generation' of the offspring, which is one higher than the maximum generation between the two parents. In December 2017 a CryptoKitty sold for \$100,000.

On March 20, 2018, it was announced that CryptoKitties would be spun off into its own company, Dapper Labs, and raised \$12 million from several top venture capital firms and angel investors like Animoca. The investment round was led by New York based Union Square Ventures and San Francisco based Andreessen Horowitz.

On May 12, 2018, a CryptoKitty was sold for \$140,000. In May 2018, CryptoKitties launched their first celebrity-branded CryptoKitty with Stephen Curry, an American professional basketball player. As part of the partnership, Curry was given three CryptoKitties with special imagery, the first of which he put up for auction. The company later suspended the auction, claiming that Stephen Curry wasn't as involved as they initially thought.[citation needed] The company was later sued for trade secret theft over the Stephen Curry collectibles. The court ruled in the company's favour, stating that "[t]he evidence demonstrates that Defendant, not Plaintiff, developed the idea to license digital collectibles using the likeness of celebrities first…".

In October 2018, CryptoKitties reached the milestone of 1 million cats being bred with a volume of 3.2 million transactions on its smart contracts. In November 2018, Dapper Labs, which was spun out of Axiom Zen as the developer of CryptoKitties, raised an additional \$15 million in a venture round led by Venrock. The company doubled its valuation in this round.

In 2018, CryptoKitties was used by the German museum ZKM Center for Art and Media Karlsruhe to showcase blockchain technology.

A CryptoKitty's ownership is tracked via a smart contract on the Ethereum blockchain. Each CryptoKitty is represented as a non-fungible token using the ERC-721 token standard on Ethereum. Generation 0 CryptoKitties were sold to players in an auction at the rate of one every 15 minutes (672 per week) for one year. New CryptoKitties are created by breeding existing CryptoKitties.

Based on the limited number of cats going into circulation and their limited genomes, there is a limit of around 4 billion total cats that can be bred. Each cat has a distinct visual appearance ("phenotype") determined by its immutable genes ("genotype") stored in the smart contract. Because cats are tokens on a blockchain, they can be bought, sold, or transferred digitally, with strong guarantees of ownership. A CryptoKitty does not have a permanently assigned gender. While they can only engage in one breeding session at one time, each cat is able to act as either matron or sire. There is a 'cooldown' time that indicates how soon the cat can breed again, which goes up with the number of breeds, capped at one week.

A group known as Axiom Zen innovation studio developed the game. Until November 2018, Axiom Zen intends to continually release a new CryptoKitty every 15 minutes, with the rest of supply determined by breeding of CryptoKitties. CryptoKitty owners may put them up for sale via an auction for a price set in Ether (ETH). They could also put them up for sire, where another player can pay to breed with a specific CryptoKitty.

## References and Further Reading for Chapter 9

http://www.proactiveinvestors.com.au/companies/news/190721/animoca-brands-one-of-the-asx-top-performers-in-2018-190721.html

https://venturebeat.com/2018/10/06/cryptokitties-explained-why-players-have-bred-over-a-million-blockchain-felines/

https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3084411

https://www.bbc.com/news/technology-42237162

https://www.cnbc.com/2017/12/06/meet-cryptokitties-the-new-digital-beanie-babies-selling-for-100k.html

https://venturebeat.com/2018/11/01/cryptokitties-creator-dapper-labs-raises-15-million-to-build-more-blockchain-cats/

https://venturebeat.com/2018/10/06/cryptokitties-explained-why-players-have-bred-over-a-million-blockchain-felines/

https://venturebeat.com/2018/08/28/cryptokitties-will-help-german-museum-explain-blockchain/

https://www.chepicap.com/en/news/3286/cryptokitties-at-german-art-exhibit-bringing-blockchain-to-life-.html

https://www.bloomberg.com/news/articles/2017-12-04/cryptokitties-quickly-becomes-most-widely-used-ethereum-app

# Appendix 1 List of Cryptocurrencies

| Currency         | Symbol       | Founder(s)                      | Hash Algorithm                              | Programming Languages     | blockchain (POS, POW, or other)  |
| ---------------- | ------------ | ------------------------------- | ------------------------------------------- | ------------------------- | -------------------------------- |
| Bitcoin          | BTC, XBT, ₿  | Satoshi Nakamoto                | SHA-256d                                    | C++                       | PoW                              |
| Litecoin         | LTC, Ł       | Charlie Lee                     | Scrypt                                      | C++                       | PoW                              |
| Namecoin         | NMC          | Vincent Durham                  | SHA-256d                                    | C++                       | PoW                              |
| Peercoin         | PPC          | Sunny King(pseudonym)           | SHA-256d                                    | C++                       | PoW & POS                        |
| Dogecoin         | DOGE, XDG, Ð | Jackson Palmer & Billy Markus   | Scrypt                                      | C++                       | PoW                              |
| Gridcoin         | GRC          | Rob Hälford                     | Scrypt                                      | C++                       | Decentralized PoS                |
| Primecoin        | XPM          | Sunny King(pseudonym)           | 1CC/2CC/TWN                                 | Typescript, C++           | PoW                              |
| Ripple           | XRP          | Chris Larsen & Jed McCaleb      | ECDSA                                       | C++                       | "Consensus"                      |
| Nxt              | NXt          | BCNext (pseudonym)              | SHA-256d                                    | Java                      | PoS                              |
| Auroracoin       | AUR          | Baldur Odinsson(pseudonym)      | Scrypt                                      | C++                       | PoW                              |
| Dash             | DASH         | Evan Duffield & Kyle Hagan      | X11                                         | C++                       | PoW & Proof of Service           |
| NEO              | NEO          | Da Hongfei & Erik Zhang         | SHA-256d & RIPEMD160                        | C#                        | dBFT                             |
| MazaCoin         | MZC          | BTC Oyate Initiative            | SHA-256d                                    | C++                       | PoW                              |
| Monero           | XMR          | Monero Core Team UtopianFuture  | CryptoNight                                 | C++                       | PoW                              |
| NEM              | XEM          | UtopianFuture(pseudonym)        | SHA3-512                                    | Java                      | PoI                              |
| PotCoin          | POT          | Potcoin core dev team           | Scrypt                                      | C++                       | PoS                              |
| Titcoin          | TIT          | Edward Mansfield & RichardAllen | SHA-256d                                    | Typescript, C++           | PoW                              |
| Verge            | XVG          | Sunerok                         | Scrypt, x17, groestl, blake2s and lyra2rev2 | C, C++                    | PoW                              |
| Stellar          | XLM          | Jed McCaleb                     | Stellar Consensus Protocol (SCP)            | C, C++                    | Stellar Consensus Protocol (SCP) |
| Vertcoin         | VTC          | Bushido                         | Lyra2RE[46]                                 | C++                       | PoW                              |
| Ethereum         | ETH          | Vitalik Buterin                 | Ethash[49]                                  | C++, GO                   | PoW                              |
| Ethereum Classic | ETC          |                                 | Ethash                                      |                           | PoW                              |
| Tether           | USDT         | Jan Ludovicus van der Velde     | Omnicore                                    |                           | PoW                              |
| Zcash            | ZEC          | Zooko Wilcox                    | Equihash                                    | C++                       | PoW                              |
| Bitcoin Cash     | BCH          | Zooko Wilcox                    | SHA-256d                                    |                           | PoW                              |
| EOS.IO           | EOS          | Dan Larimer                     | Equihash                                    | WebAssembly, Rust, C, C++ | delegated PoS                    |
